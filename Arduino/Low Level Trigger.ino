int triggerPin=13;
 
void setup() {
  
  pinMode(triggerPin,OUTPUT);
 
}
 
void loop() {
 
  digitalWrite(triggerPin,HIGH);
  delay(1000);
  digitalWrite(triggerPin,LOW);
  delay(1000);
 
}
