#include <Servo.h>  
#include <SPI.h>  
#include <SD.h> 
  
const int sensor=A0; 
  
int chipSelect=4; 
File mySensorData; 
  
Servo myServo; 
const int servoPin=9; 
 
int vibrationSensor=A2;
  
int lightSensor=A1; 
  
void setup() { 
   
  pinMode(sensor,INPUT); 
  
  pinMode(lightSensor,INPUT); 
   
  Serial.begin(9600); 
  
  pinMode(10,OUTPUT); 
  SD.begin(chipSelect); 
  myServo.attach(servoPin); 
 
  pinMode(vibrationSensor,INPUT);
  
} 
  
void loop() { 
  
  mySensorData=SD.open("SEP.txt",FILE_WRITE);  
  
if (mySensorData) {
 
int reading=analogRead(sensor); 
int pos = (155./1023.)*reading+15;  
  myServo.write(pos); 
 
int vibData=analogRead(vibrationSensor);
  
int data=analogRead(lightSensor);
   
  Serial.print("Servo position is: "); 
  Serial.println(reading); 
  delay(250); 
  
  delay (250);  
  Serial.print("The amount of vibration is:");  
  Serial.println(vibData); 
  
  Serial.print("Amount of light is: "); 
  Serial.println(data); 
  delay(250); 
  
  Serial.println(" "); 
  
  mySensorData.print("Servo position is: "); 
  mySensorData.println(reading); 
  delay(250); 
  
  delay (1000);  
  mySensorData.print("The amount of vibration is: ");  
  mySensorData.println(vibData); 
  
  mySensorData.print("Amount of light is: "); 
  mySensorData.println(data); 
  delay(250); 
  
  mySensorData.println(" "); 
   
  mySensorData.close(); 
   
}   
}
