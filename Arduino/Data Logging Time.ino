#include "Wire.h"
#include <SPI.h>  
#include <SD.h> 

#define DS3231_I2C_ADDRESS 0x68   
    
byte decToBcd(byte val)   
{   
  return( (val/10*16) + (val%10) );   
}   
    
byte bcdToDec(byte val)   
{   
  return( (val/16*10) + (val%16) );   
} 
 
int chipSelect=4; 
File mySensorData; 
  
void setup() {     

  pinMode(10,OUTPUT); 
 
  SD.begin(chipSelect);
        
  Wire.begin();   
  Serial.begin(9600);   
  // set the initial time here:   
  // DS3231 seconds, minutes, hours, day, date, month, year   
 // setDS3231time(00,50,11,07,20,05,17);  
     
}   
void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte   
dayOfMonth, byte month, byte year)   
{   
  // sets time and date data to DS3231   
  Wire.beginTransmission(DS3231_I2C_ADDRESS);   
  Wire.write(0); // set next input to start at the seconds register   
  Wire.write(decToBcd(second)); // set seconds   
  Wire.write(decToBcd(minute)); // set minutes   
  Wire.write(decToBcd(hour)); // set hours   
  Wire.write(decToBcd(dayOfWeek)); // set day of week (1=Sunday, 7=Saturday)   
  Wire.write(decToBcd(dayOfMonth)); // set date (1 to 31)   
  Wire.write(decToBcd(month)); // set month   
  Wire.write(decToBcd(year)); // set year (0 to 99)   
  Wire.endTransmission();   
}   
void readDS3231time(byte *second,   
byte *minute,   
byte *hour,   
byte *dayOfWeek,   
byte *dayOfMonth,   
byte *month,   
byte *year)   
{   
  Wire.beginTransmission(DS3231_I2C_ADDRESS);   
  Wire.write(0); // set DS3231 register pointer to 00h   
  Wire.endTransmission();   
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);   
  // request seven bytes of data from DS3231 starting from register 00h   
  *second = bcdToDec(Wire.read() & 0x7f);   
  *minute = bcdToDec(Wire.read());   
  *hour = bcdToDec(Wire.read() & 0x3f);   
  *dayOfWeek = bcdToDec(Wire.read());   
  *dayOfMonth = bcdToDec(Wire.read());   
  *month = bcdToDec(Wire.read());   
  *year = bcdToDec(Wire.read());   
}   
void displayTime()   
{   
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;   
  // retrieve data from DS3231   
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month,   
  &year);   
  // send it to the serial monitor   
  mySensorData.print(hour, DEC);   
  // convert the byte variable to a decimal number when displayed   
  mySensorData.print(":");   
  if (minute<10)   
  {   
    mySensorData.print("0");   
  }   
  mySensorData.print(minute, DEC);   
  mySensorData.print(":");   
  if (second<10)   
  {   
    mySensorData.print("0");   
  }   
  mySensorData.print(second, DEC);   
  mySensorData.print(" ");   
  mySensorData.print(dayOfMonth, DEC);   
  mySensorData.print("/");   
  mySensorData.print(month, DEC);   
  mySensorData.print("/");   
  mySensorData.print(year, DEC);   
  mySensorData.print(" Day of week: ");   
  switch(dayOfWeek){   
  case 1:   
    mySensorData.println("Sunday");   
    break;   
  case 2:   
    mySensorData.println("Monday");   
    break;   
  case 3:   
    mySensorData.println("Tuesday");   
    break;   
  case 4:   
    mySensorData.println("Wednesday");   
    break;   
  case 5:   
    mySensorData.println("Thursday");   
    break;   
  case 6:   
    mySensorData.println("Friday");   
    break;   
  case 7:   
    mySensorData.println("Saturday");   
    break;   
  }  
  mySensorData.println("");    
} 
  
void loop() { 
 
  mySensorData=SD.open("TIME.txt",FILE_WRITE); 
  
if (mySensorData) {  

  displayTime();
  Serial.println("Boo yay!!!");
  mySensorData.close();

} 
}

/*
Pull battery if RTC doesn't work!:) 
*/
