int trgPin=13;
int echoPin=11;
float pingTime;
float speedOfSound=739.50;
float targetDistance;

void setup() {
  
  Serial.begin(9600);
  pinMode (trgPin,OUTPUT);
  pinMode (echoPin,INPUT);  
}

void loop() {
  
  digitalWrite(trgPin,LOW);
  delayMicroseconds (2000);
  digitalWrite(trgPin,HIGH);
  delayMicroseconds (10);
  digitalWrite(trgPin,LOW);
  pingTime=pulseIn(echoPin,HIGH);
  pingTime=pingTime/1000000.;
  pingTime=pingTime/3600.;
  targetDistance=speedOfSound*pingTime;
  targetDistance=targetDistance/2;
  targetDistance=targetDistance*63360;
  delay (1000);
  Serial.println("The distance of the target is:");
  Serial.print(targetDistance);
  
}

