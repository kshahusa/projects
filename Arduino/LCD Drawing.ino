#include <LiquidCrystal.h>
LiquidCrystal lcd(12, 11, 2, 7, 8, 9, 10);

uint8_t custom_hex1[8] = {0x04,0x04,0x0E,0x11,0x11,0x0E,0x04,0x04};  //a custom HEX character

void setup()
{
  lcd.begin(16, 2);

  lcd.clear();
  
  lcd.createChar(1, custom_hex1);
  
  lcd.home();
  
  lcd.write((byte)1);
  
}
void loop()
{
}

