int led_1=2;
int led_2=3;
int led_3=4;

long randomNumber;

void setup() {
  
  Serial.begin(9600);
  pinMode(led_1,OUTPUT);
  pinMode(led_2,OUTPUT);
  pinMode(led_3,OUTPUT);
  Serial.println("DIABO vs. BUENO");
  randomSeed(analogRead(A0));

}

void loop() {

  randomNumber = random(2,5);
  Serial.print("The random number is: ");
  Serial.print(randomNumber);
  Serial.println(" ");
  delay(500);

  digitalWrite(randomNumber,HIGH);
  delay(500);
  digitalWrite(randomNumber,LOW);

}

