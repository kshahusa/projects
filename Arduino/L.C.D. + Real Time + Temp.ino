#include <LiquidCrystal.h>
#include "Wire.h"      
#include <SPI.h>
 
LiquidCrystal LCD(10,9,5,4,3,2);
 
const int sensorPin = A0;
 
int counter=1000;
   
#define DS3231_I2C_ADDRESS 0x68  
   
byte decToBcd(byte val)  
{  
  return( (val/10*16) + (val%10) );  
}  
   
byte bcdToDec(byte val)  
{  
  return( (val/16*10) + (val%16) );  
}  
     
void setup() { 
     
  LCD.begin(16,2);
  LCD.setCursor(0,0);
  Wire.begin();    
  // set the initial time here:  
  // DS3231 seconds, minutes, hours, day, date, month, year  
  //setDS3231time(00,24,15,07,27,05,17); 
    
}  
void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte  
dayOfMonth, byte month, byte year)  
{  
  // sets time and date data to DS3231  
  Wire.beginTransmission(DS3231_I2C_ADDRESS);  
  Wire.write(0); // set next input to start at the seconds register  
  Wire.write(decToBcd(second)); // set seconds  
  Wire.write(decToBcd(minute)); // set minutes  
  Wire.write(decToBcd(hour)); // set hours  
  Wire.write(decToBcd(dayOfWeek)); // set day of week (1=Sunday, 7=Saturday)  
  Wire.write(decToBcd(dayOfMonth)); // set date (1 to 31)  
  Wire.write(decToBcd(month)); // set month  
  Wire.write(decToBcd(year)); // set year (0 to 99)  
  Wire.endTransmission();  
}  
void readDS3231time(byte *second,  
byte *minute,  
byte *hour,  
byte *dayOfWeek,  
byte *dayOfMonth,  
byte *month,  
byte *year)  
{  
  Wire.beginTransmission(DS3231_I2C_ADDRESS);  
  Wire.write(0); // set DS3231 register pointer to 00h  
  Wire.endTransmission();  
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);  
  // request seven bytes of data from DS3231 starting from register 00h  
  *second = bcdToDec(Wire.read() & 0x7f);  
  *minute = bcdToDec(Wire.read());  
  *hour = bcdToDec(Wire.read() & 0x3f);  
  *dayOfWeek = bcdToDec(Wire.read());  
  *dayOfMonth = bcdToDec(Wire.read());  
  *month = bcdToDec(Wire.read());  
  *year = bcdToDec(Wire.read());  
}  
void displayTime()  
{  
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;  
  // retrieve data from DS3231  
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month,  
  &year);  
  // send it to the serial monitor  
  LCD.print(hour, DEC);  
  // convert the byte variable to a decimal number when displayed  
  LCD.print(":");  
  if (minute<10)  
  {  
    LCD.print("0");  
  }  
  LCD.print(minute, DEC);
  //Serial.print(":");  
  //if (second<10)  
  //{  
    //Serial.print("0");  
  //}  
  //Serial.print(second, DEC);    
  LCD.print(" ");  
  LCD.print(dayOfMonth, DEC);  
  LCD.print("/");  
  LCD.print(month, DEC);  
  LCD.print("/");  
  LCD.print(year, DEC);  
//  LCD.print(" Day of week: ");
  LCD.setCursor(0,1);
  switch(dayOfWeek){  
  case 1:  
    LCD.println("Sunday");  
    break;  
  case 2:  
    LCD.println("Monday");  
    break;  
  case 3:  
    LCD.println("Tuesday");  
    break;  
  case 4:  
    LCD.println("Wednesday");  
    break;  
  case 5:  
    LCD.println("Thursday");  
    break;  
  case 6:  
    LCD.println("Friday");  
    break;  
  case 7:  
    LCD.println("Saturday");  
    break;  
  }    
}    
     
void loop() {      
 
int reading = analogRead(sensorPin);   
 
float voltage = reading * 5.0; 
voltage /= 1024.0;  
 
float temperatureC = (0.5 - voltage) * 100. ;    
  
float temperatureF = (temperatureC * 9.0 / 5.0) + 32.0; 
 
delay(1000);
    
  LCD.setCursor(0,0);
  displayTime(); 
  LCD.setCursor(9,1);
  LCD.print("!");
  LCD.setCursor(10,1);
  LCD.print(" ");
 
  delay(10000);
  LCD.clear();
   
  LCD.setCursor(0,0);
  LCD.print("Temperature is:");
  LCD.setCursor(0,1);
  LCD.print(temperatureF);
 
  delay(counter);
  LCD.clear();
 
  counter+1000;
if (counter>=10000) {
  counter+0;
}
 
}   
