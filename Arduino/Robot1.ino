#include <Servo.h>
#include <Stepper.h> 
 
const int stepsPerRevolution = 100;  
 
Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11); 
Servo myServo;
 
const int servoPin=7; 
const int servoPosition=150; 
 
int trgPin=13; 
int echoPin=12; 
 
float pingTime; 
float speedOfSound=739.50; 
float targetDistance;
 
int stepCount = 0;  
int sensorPin = A0; 
 
int buzzer=6;
 
void setup () {
 
  Serial.begin (9600);
 
  pinMode(trgPin,OUTPUT); 
  pinMode(echoPin,INPUT);
  pinMode(buzzer,OUTPUT);
  
}
 
void loop () {
 
const int targetDistance=1;
 
for(int i=1; i>=70;i++) {
 
  int sensorReading = analogRead(A0);  
  int motorSpeed = map(sensorReading, 0, 1023, 0, 100); 
 
if (motorSpeed > 0) { 
    myStepper.setSpeed(motorSpeed);  
   myStepper.step(stepsPerRevolution / 100); 
}
}
 
if (targetDistance<=2) {
  
  myServo.write(servoPosition);
 
} 
  analogWrite(buzzer,255);
  delay(500);
  analogWrite(buzzer,100);
  delay(500);
  analogWrite(buzzer,255);
  delay(500);
  analogWrite(buzzer,200);
  delay(500); 
}
