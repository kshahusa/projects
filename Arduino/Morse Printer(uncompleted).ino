#include <Stepper.h>
#include <Servo.h>
#include <Wire.h> 
#include <LCD.h> 
#include <LiquidCrystal_I2C.h> 
  
#define I2C_ADDR    0x3F 
#define BACKLIGHT_PIN     3 
#define En_pin  2 
#define Rw_pin  1 
#define Rs_pin  0 
#define D4_pin  4 
#define D5_pin  5 
#define D6_pin  6 
#define D7_pin  7 
  
int n = 1; 
  
LiquidCrystal_I2C  lcd(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin); 
Servo myservo;
 
const int servo_pin = 6;
const int down = 170;
const int up = 0;
const int line = 250;
const int dot = 50;
 
const int low_level_trig_pin = 2;
 
const int potpin = A0;
 
const int stepsPerRevolution = 200;
 
Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11);
 
int stepCount = 0;  
 
void setup() {
 
  myservo.attach(6);
 
  pinMode(low_level_trig_pin,OUTPUT);
  
}
 
void loop() {
 
  int sensorReading = analogRead(potpin);
  int motorSpeed = map(sensorReading, 0, 1023, 0, 100);
  
  if (motorSpeed > 0) {
    myStepper.setSpeed(motorSpeed);
    myStepper.step(stepsPerRevolution / 100);
  } 
}
