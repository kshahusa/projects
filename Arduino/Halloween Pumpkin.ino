#include "pitches.h"
 
const int buzzerPin = 8;
const int led_mouth = 3;
const int led_eye1 = 5;
const int led_eye2 = 6;
const int red = 9;
const int green = 10;
const int blue = 11;
int counter = 0;
 
void bat_noise() {
 
for(int i = 1;i <= 20;i++) {   
    
    tone(buzzerPin,2200); // then buzz by going high
    tone(buzzerPin,1000);
    tone(buzzerPin,500);
    tone(buzzerPin,200);
    tone(buzzerPin,500);
    delayMicroseconds(10000);    // waiting
    noTone(buzzerPin);  // going low
    delayMicroseconds(10000);    // and waiting more
    tone(buzzerPin,2200); 
    tone(buzzerPin,1000);
    delayMicroseconds(10000);    // waiting
    noTone(buzzerPin);  // going low
    delayMicroseconds(10000);    // and waiting more
    tone(buzzerPin,100); 
    delayMicroseconds(10000);    // waiting
    noTone(buzzerPin);  // going low
    delayMicroseconds(10000);    // and waiting more
    tone(buzzerPin,100); 
    delayMicroseconds(10000);    // waiting
    noTone(buzzerPin);  // going low
    delayMicroseconds(10000);    // and waiting more
}
}
 
bool led_spook_on() {
  
  for(counter = 0;counter <= 255;counter++) {
   
   analogWrite(led_mouth,counter); 
   delay(3);
   analogWrite(led_eye1,counter);
   delay(3);
   analogWrite(led_eye2,counter);
   delay(3);
  }
  return true;
}
 
void rgb_colors() {
 
  analogWrite(red,255);
  delay(1000);
  analogWrite(red,0);
  delay(1000);
  analogWrite(green,255);
  delay(1000);
  analogWrite(green,0);
  delay(1000);
  analogWrite(blue,255);
  delay(1000);
  analogWrite(blue,0);
  delay(1000);
  
}
 
void setup(){
 
  pinMode(buzzerPin,OUTPUT);
  
}
 
void loop(){
 
  led_spook_on();
 
if(led_spook_on() == true) { 
 
  bat_noise();
  rgb_colors();
 
}   
}
