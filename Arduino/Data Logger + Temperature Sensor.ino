#include <SPI.h> 
#include <SD.h> 
 
int sensorPin = 0;
  
int chipSelect=4;
 
File mySensorData; 
  
void setup() { 
   
  Serial.begin(9600); 
  
  pinMode(10,OUTPUT); 
  SD.begin(chipSelect); 
  
} 
  
void loop() { 
  
int reading = analogRead(sensorPin);  
 
float voltage = reading * 5.0;
voltage /= 1024.0; 
 
float temperatureC = (0.5 - voltage) * 100. ;  
float temperatureF = (temperatureC * 9.0 / 5.0) + 32.0; 
  
  mySensorData=SD.open("Temp.txt",FILE_WRITE); 
  
if (mySensorData) {
 
  Serial.print("Temperature Fahrenheit: ");
  Serial.println(temperatureF);
  Serial.println("");
  Serial.print("Temperature Celcius: ");
  Serial.println(temperatureC);
  delay(1000);
  
  mySensorData.print(temperatureF); 
  mySensorData.print(","); 
  mySensorData.print(" "); 
  mySensorData.println(temperatureC); 
  mySensorData.print(""); 
  mySensorData.close(); 
   
}
}
