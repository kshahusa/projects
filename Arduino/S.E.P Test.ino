#include "Wire.h"
#include <Servo.h>   
#include <SPI.h>   
#include <SD.h>  
   
const int sensor=A0;  
   
int chipSelect=4;  
File mySensorData;  
   
Servo myServo;  
const int servoPin=9;  
  
int vibrationSensor=A2; 
   
int lightSensor=A1;
 
#define DS3231_I2C_ADDRESS 0x68
 
byte decToBcd(byte val)
{
  return( (val/10*16) + (val%10) );
}
 
byte bcdToDec(byte val)
{
  return( (val/16*10) + (val%16) );
}
   
void setup() {  
    
  pinMode(sensor,INPUT);  
   
  pinMode(lightSensor,INPUT);  
    
  Serial.begin(9600);  
   
  pinMode(10,OUTPUT);  
  SD.begin(chipSelect);  
  myServo.attach(servoPin);  
  
  pinMode(vibrationSensor,INPUT);
 
  Wire.begin();
  Wire.begin();
  Serial.begin(9600);
  // set the initial time here:
  // DS3231 seconds, minutes, hours, day, date, month, year
  //setDS3231time(00,31,14,01,30,04,17);
}
void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte
dayOfMonth, byte month, byte year)
{
  // sets time and date data to DS3231
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set next input to start at the seconds register
  Wire.write(decToBcd(second)); // set seconds
  Wire.write(decToBcd(minute)); // set minutes
  Wire.write(decToBcd(hour)); // set hours
  Wire.write(decToBcd(dayOfWeek)); // set day of week (1=Sunday, 7=Saturday)
  Wire.write(decToBcd(dayOfMonth)); // set date (1 to 31)
  Wire.write(decToBcd(month)); // set month
  Wire.write(decToBcd(year)); // set year (0 to 99)
  Wire.endTransmission();
}
void readDS3231time(byte *second,
byte *minute,
byte *hour,
byte *dayOfWeek,
byte *dayOfMonth,
byte *month,
byte *year)
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set DS3231 register pointer to 00h
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);
  // request seven bytes of data from DS3231 starting from register 00h
  *second = bcdToDec(Wire.read() & 0x7f);
  *minute = bcdToDec(Wire.read());
  *hour = bcdToDec(Wire.read() & 0x3f);
  *dayOfWeek = bcdToDec(Wire.read());
  *dayOfMonth = bcdToDec(Wire.read());
  *month = bcdToDec(Wire.read());
  *year = bcdToDec(Wire.read());
}
void displayTime()
{
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  // retrieve data from DS3231
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month,
  &year);
  // send it to the serial monitor
  Serial.print(hour, DEC);
  // convert the byte variable to a decimal number when displayed
  Serial.print(":");
  if (minute<10)
  {
    Serial.print("0");
  }
  Serial.print(minute, DEC);
  Serial.print(":");
  if (second<10)
  {
    Serial.print("0");
  }
  Serial.print(second, DEC);
  Serial.print(" ");
  Serial.print(dayOfMonth, DEC);
  Serial.print("/");
  Serial.print(month, DEC);
  Serial.print("/");
  Serial.print(year, DEC);
  Serial.print(" Day of week: ");
  switch(dayOfWeek){
  case 1:
    Serial.println("Sunday");
    break;
  case 2:
    Serial.println("Monday");
    break;
  case 3:
    Serial.println("Tuesday");
    break;
  case 4:
    Serial.println("Wednesday");
    break;
  case 5:
    Serial.println("Thursday");
    break;
  case 6:
    Serial.println("Friday");
    break;
  case 7:
    Serial.println("Saturday");
    break;
  }  
}  
   
void loop() {  
   
  mySensorData=SD.open("SEP.txt",FILE_WRITE);   
   
if (mySensorData) { 
  
int reading=analogRead(sensor);  
int pos = (155./1023.)*reading+15;   
  myServo.write(pos);  
  
int vibData=analogRead(vibrationSensor); 
   
int data=analogRead(lightSensor); 
    
  Serial.print("Servo position is: ");  
  Serial.println(reading);  
  delay(250);  
   
  delay (250);   
  Serial.print("The amount of vibration is:");   
  Serial.println(vibData);  
   
  Serial.print("Amount of light is: ");  
  Serial.println(data);  
  delay(250); 

  displayTime(); 
  delay(1000);  
   
  Serial.println(" ");  
   
  mySensorData.print("Servo position is: ");  
  mySensorData.println(reading);  
  delay(250);  
   
  delay (1000);   
  mySensorData.print("The amount of vibration is: ");   
  mySensorData.println(vibData);  
   
  mySensorData.print("Amount of light is: ");  
  mySensorData.println(data);  
  delay(250);  
   
  mySensorData.println(" ");  
    
  mySensorData.close();  
    
}    
} 
