#include <SPI.h>
 
#include <SD.h>
 
int sensor=A0;
 
int chipSelect=4;
File mySensorData;
 
void setup() {
  
  Serial.begin(9600);

  pinMode(sensor,INPUT);
  pinMode(10,OUTPUT);

  SD.begin(chipSelect);
 
}
 
void loop() {
 
int reading=analogRead(sensor);
 
  mySensorData=SD.open("K.txt",FILE_WRITE);
 
if (mySensorData) { 
  
  Serial.print("The amount of light is: ");
  Serial.println(reading);
  delay(250);
 
  mySensorData.print(reading);
  mySensorData.print(",");
  mySensorData.print(" ");
  mySensorData.println(reading);
  mySensorData.print("");
  mySensorData.close();
  
}
}
