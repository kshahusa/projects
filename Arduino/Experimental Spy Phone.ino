#include <LiquidCrystal.h>
#include <Wire.h>       
#include <SPI.h>       
#include <AddicoreRFID.h> 
 
LiquidCrystal lcd(7,6,5,4,3,2);
    
#define DS3231_I2C_ADDRESS 0x68   
    
byte decToBcd(byte val)   
{   
  return( (val/10*16) + (val%10) );   
}   
    
byte bcdToDec(byte val)   
{   
  return( (val/16*10) + (val%16) );   
}   
// Example sketch to read the ID from an Addicore 13.56MHz RFID tag 
// as found in the RFID AddiKit found at:  
// http://www.addicore.com/RFID-AddiKit-with-RC522-MIFARE-Module-RFID-Cards-p/126.htm 
 
#define uchar unsigned char 
#define uint unsigned int 
 
uchar fifobytes; 
uchar fifoValue; 
 
AddicoreRFID myRFID; // create AddicoreRFID object to control the RFID module 
 
///////////////////////////////////////////////////////////////////// 
//set the pins 
///////////////////////////////////////////////////////////////////// 
const int chipSelectPin = 10; 
const int NRSTPD = 5; 
 
//Maximum length of the array 
#define MAX_LEN 16 
      
void setup() {     
 
  Wire.begin();   
  Serial.begin(9600);
  lcd.begin(16,2);
  lcd.setCursor(0,0);
  lcd.print("Scan card!");
 
  // start the SPI library: 
  SPI.begin(); 
   
  pinMode(chipSelectPin,OUTPUT);              // Set digital pin 10 as OUTPUT to connect it to the RFID /ENABLE pin  
    digitalWrite(chipSelectPin, LOW);         // Activate the RFID reader 
  pinMode(NRSTPD,OUTPUT);                     // Set digital pin 10 , Not Reset and Power-down 
    digitalWrite(NRSTPD, HIGH); 
 
  myRFID.AddicoreRFID_Init(); 
 
  uchar i, tmp, checksum1; 
uchar status; 
        uchar str[MAX_LEN]; 
        uchar RC_size; 
        uchar blockAddr; //Selection operation block address 0 to 63 
        String mynum = ""; 
 
 
        str[1] = 0x4400; 
//Find tags, return tag type 
status = myRFID.AddicoreRFID_Request(PICC_REQIDL, str);
  
          Serial.println("RFID tag detected"); 
          Serial.print("Tag Type:\t\t"); 
          uint tagType = str[0] << 8; 
          tagType = tagType + str[1]; 
          switch (tagType) { 
            case 0x4400: 
              Serial.println("Mifare UltraLight"); 
              break; 
            case 0x400: 
              Serial.println("Mifare One (S50)"); 
              break; 
            case 0x200: 
              Serial.println("Mifare One (S70)"); 
              break; 
            case 0x800: 
              Serial.println("Mifare Pro (X)"); 
              break; 
            case 0x4403: 
              Serial.println("Mifare DESFire"); 
              break; 
            default: 
              Serial.println("Unknown"); 
              break; 
          } 
 
//Anti-collision, return tag serial number 4 bytes 
status = myRFID.AddicoreRFID_Anticoll(str); 
if(status != MI_OK) {
  lcd.print("Scan card!");
}
else{ 
          checksum1 = str[0] ^ str[1] ^ str[2] ^ str[3]; 
          Serial.print("The tag's number is:\t"); 
       Serial.print(str[0]); 
            Serial.print(" , "); 
       Serial.print(str[1]); 
            Serial.print(" , "); 
       Serial.print(str[2]); 
            Serial.print(" , "); 
       Serial.println(str[3]); 
 
 
          Serial.print("Read Checksum:\t\t"); 
         Serial.println(str[4]); 
          Serial.print("Calculated Checksum:\t"); 
            Serial.println(checksum1); 
             
            // Should really check all pairs, but for now we'll just use the first 
            if(str[0] == 158)                      //You can change this to the first byte of your tag by finding the card's ID through the Serial Monitor 
            { 
                lcd.print("Hello Ami!"); 
            } else if(str[0] == 181) {             //You can change this to the first byte of your tag by finding the card's ID through the Serial Monitor 
                lcd.print("Hello Krishna!"); 
            } 
            Serial.println(); 
            delay(1000); 
} 
        myRFID.AddicoreRFID_Halt();   //Command tag into hibernation
}           
  // set the initial time here:   
  // DS3231 seconds, minutes, hours, day, date, month, year   
// setDS3231time(00,50,11,07,20,05,17);  
     
   
void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte   
dayOfMonth, byte month, byte year)   
{   
  // sets time and date data to DS3231   
  Wire.beginTransmission(DS3231_I2C_ADDRESS);   
  Wire.write(0); // set next input to start at the seconds register   
  Wire.write(decToBcd(second)); // set seconds   
  Wire.write(decToBcd(minute)); // set minutes   
  Wire.write(decToBcd(hour)); // set hours   
  Wire.write(decToBcd(dayOfWeek)); // set day of week (1=Sunday, 7=Saturday)   
  Wire.write(decToBcd(dayOfMonth)); // set date (1 to 31)   
  Wire.write(decToBcd(month)); // set month   
  Wire.write(decToBcd(year)); // set year (0 to 99)   
  Wire.endTransmission();   
}   
void readDS3231time(byte *second,   
byte *minute,   
byte *hour,   
byte *dayOfWeek,   
byte *dayOfMonth,   
byte *month,   
byte *year)   
{   
  Wire.beginTransmission(DS3231_I2C_ADDRESS);   
  Wire.write(0); // set DS3231 register pointer to 00h   
  Wire.endTransmission();   
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);   
  // request seven bytes of data from DS3231 starting from register 00h   
  *second = bcdToDec(Wire.read() & 0x7f);   
  *minute = bcdToDec(Wire.read());   
  *hour = bcdToDec(Wire.read() & 0x3f);   
  *dayOfWeek = bcdToDec(Wire.read());   
  *dayOfMonth = bcdToDec(Wire.read());   
  *month = bcdToDec(Wire.read());   
  *year = bcdToDec(Wire.read());   
}   
void displayTime()   
{   
  lcd.clear();
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;   
  // retrieve data from DS3231   
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month,   
  &year);   
  // send it to the serial monitor   
  lcd.print(hour, DEC);   
  // convert the byte variable to a decimal number when displayed   
  lcd.print(":");   
  if (minute<10)   
  {   
    lcd.print("0");   
  }   
  lcd.print(minute, DEC);   
  lcd.print(":");   
  if (second<10)   
  {   
    lcd.print("0");   
  }   
  lcd.print(second, DEC);   
  lcd.print(" ");   
  lcd.print(dayOfMonth, DEC);   
  lcd.print("/");   
  lcd.print(month, DEC);   
  lcd.print("/");   
  lcd.print(year, DEC);   
  Serial.print(" Day of week: ");
  lcd.setCursor(0,1);   
  switch(dayOfWeek){   
  case 1:   
    lcd.print("Sunday");   
    break;   
  case 2:   
    lcd.print("Monday");   
    break;   
  case 3:   
    lcd.print("Tuesday");   
    break;   
  case 4:   
    lcd.print("Wednesday");   
    break;   
  case 5:   
    lcd.print("Thursday");   
    break;   
  case 6:   
    lcd.print("Friday");   
    break;   
  case 7:   
    lcd.print("Saturday");   
    break;   
  }  
  Serial.println("");    
}     
      
void loop() {        
   
  displayTime();  
         
}    

