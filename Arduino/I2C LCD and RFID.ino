// Example sketch to read the ID from an Addicore 13.56MHz RFID tag 
// as found in the RFID AddiKit found at:  
// http://www.addicore.com/RFID-AddiKit-with-RC522-MIFARE-Module-RFID-Cards-p/126.htm 
 
 
#include <AddicoreRFID.h> 
#include <SPI.h> 
#include <Wire.h> 
#include <LCD.h> 
#include <LiquidCrystal_I2C.h> 
 
String common_pass = "";
String laptop_pass = "";
 
LiquidCrystal_I2C lcd(0x3F, 16, 2); 
 
#define uchar unsigned char 
#define uint unsigned int 
 
uchar fifobytes; 
uchar fifoValue; 
 
AddicoreRFID myRFID; // create AddicoreRFID object to control the RFID module 
 
const int chipSelectPin = 10; 
const int NRSTPD = 5; 
 
#define MAX_LEN 16 
 
void setup() {                 
   Serial.begin(9600);   
   
  SPI.begin(); 
   
  pinMode(chipSelectPin,OUTPUT);               
    digitalWrite(chipSelectPin, LOW);          
  pinMode(NRSTPD,OUTPUT);                      
    digitalWrite(NRSTPD, HIGH); 
 
  myRFID.AddicoreRFID_Init();
 
lcd.begin ();  
  
lcd.backlight();
 
} 
 
 
void loop() 
{ 
  uchar i, tmp, checksum1; 
uchar status; 
        uchar str[MAX_LEN]; 
        uchar RC_size; 
        uchar blockAddr;  
        String mynum = ""; 
 
 
        str[1] = 0x4400;  
status = myRFID.AddicoreRFID_Request(PICC_REQIDL, str);  
if (status == MI_OK) 
{ 
          Serial.println("RFID tag detected"); 
          Serial.print("Tag Type:\t\t"); 
          uint tagType = str[0] << 8; 
          tagType = tagType + str[1]; 
          switch (tagType) { 
            case 0x4400: 
              Serial.println("Mifare UltraLight"); 
              break; 
            case 0x400: 
              Serial.println("Mifare One (S50)"); 
              break; 
            case 0x200: 
              Serial.println("Mifare One (S70)"); 
              break; 
            case 0x800: 
              Serial.println("Mifare Pro (X)"); 
              break; 
            case 0x4403: 
              Serial.println("Mifare DESFire"); 
              break; 
            default: 
              Serial.println("Unknown"); 
              break; 
          } 
} 
  
status = myRFID.AddicoreRFID_Anticoll(str); 
if (status == MI_OK) 
{ 
          checksum1 = str[0] ^ str[1] ^ str[2] ^ str[3]; 
          Serial.print("The tag's number is:\t"); 
       Serial.print(str[0]); 
            Serial.print(" , "); 
       Serial.print(str[1]); 
            Serial.print(" , "); 
       Serial.print(str[2]); 
            Serial.print(" , "); 
       Serial.println(str[3]); 
 
 
          Serial.print("Read Checksum:\t\t"); 
         Serial.println(str[4]); 
          Serial.print("Calculated Checksum:\t"); 
            Serial.println(checksum1); 
             
           
            if(str[0] == 158)                       
            { 
                lcd.clear();
                lcd.print("\nLaptop Admin:\n"); 
                lcd.setCursor(0,1);
                lcd.print(laptop_pass);
            } else if(str[0] == 181) {
                lcd.clear(); 
                lcd.print("\nCommon:\n"); 
                lcd.setCursor(0,1);
                lcd.print(common_pass); 
            } 
            Serial.println(); 
            delay(1000); 
} 
        myRFID.AddicoreRFID_Halt();   //Command tag into hibernation 
 
       Serial.println("Enter passkey to enter advanced edit options!");
       String password = Serial.readString();
     if(password=="041907") {
      Serial.println("");
      Serial.println("Scan master card!");
     if(str[0] == 181 && str[1] == 253 && str[2] == 42 && str[3] == 166){
      Serial.println("Enter new laptop password!");
laptop_pass = Serial.readString();
      Serial.println("Enter new common password!");
common_pass = Serial.readString();
     }
     }
} 
 
/* 
Connection: 
 
SDA = 10 
SCK = 13 
MOSI = 11 
MISO = 12 
3.3V = 3.3 
GND = GND 
RST = 9 
*/ 
