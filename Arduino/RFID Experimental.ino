/*
* RFID.cpp - Library to use ARDUINO RFID MODULE KIT 13.56 MHZ WITH TAGS SPI W AND R BY COOQROBOT.
* Based on code Dr.Leong   ( WWW.B2CQSHOP.COM )
* Created by Miguel Balboa, Jan, 2012. 
* Released into the public domain.
*/
 
/******************************************************************************
* Includes
******************************************************************************/
#include <Arduino.h>
#include <RFID.h>
 
/******************************************************************************
* User API
******************************************************************************/
 
/**
* Construct RFID
* int chipSelectPin RFID /ENABLE pin
*/
RFID::RFID(int chipSelectPin, int NRSTPD)
{
  _chipSelectPin = chipSelectPin;
 
  pinMode(_chipSelectPin,OUTPUT);     // Set digital as OUTPUT to connect it to the RFID /ENABLE pin 
  digitalWrite(_chipSelectPin, LOW); 
 
 
  pinMode(NRSTPD,OUTPUT);         // Set digital pin, Not Reset and Power-down
  digitalWrite(NRSTPD, HIGH);
  _NRSTPD = NRSTPD;
}
/******************************************************************************
* User API
******************************************************************************/
 
bool RFID::isCard() 
{
  unsigned char status;
  unsigned char str[MAX_LEN];
 
  status = MFRC522Request(PICC_REQIDL, str);  
    if (status == MI_OK) {
    return true;
  } else { 
    return false; 
  }
}
 
bool RFID::readCardSerial(){
 
  unsigned char status;
  unsigned char str[MAX_LEN];
 
  // Anti-colisi�n, devuelva el n�mero de serie de tarjeta de 4 bytes
  status = anticoll(str);
  memcpy(serNum, str, 5);
 
  if (status == MI_OK) {
    return true;
  } else {
    return false;
  }
 
}
 
/******************************************************************************
* Dr.Leong   ( WWW.B2CQSHOP.COM )
******************************************************************************/
 
void RFID::init()
{
    digitalWrite(_NRSTPD,HIGH);
 
  reset();
 
  //Timer: TPrescaler*TreloadVal/6.78MHz = 24ms
    writeMFRC522(TModeReg, 0x8D);   //Tauto=1; f(Timer) = 6.78MHz/TPreScaler
    writeMFRC522(TPrescalerReg, 0x3E);  //TModeReg[3..0] + TPrescalerReg
    writeMFRC522(TReloadRegL, 30);           
    writeMFRC522(TReloadRegH, 0);
 
  writeMFRC522(TxAutoReg, 0x40);    //100%ASK
  writeMFRC522(ModeReg, 0x3D);    // CRC valor inicial de 0x6363
 
  //ClearBitMask(Status2Reg, 0x08); //MFCrypto1On=0
  //writeMFRC522(RxSelReg, 0x86);   //RxWait = RxSelReg[5..0]
  //writeMFRC522(RFCfgReg, 0x7F);     //RxGain = 48dB
 
  antennaOn();    //Abre  la antena
 
 
}
void RFID::reset()
{
  writeMFRC522(CommandReg, PCD_RESETPHASE);
}
 
void RFID::writeMFRC522(unsigned char addr, unsigned char val)
{
  digitalWrite(_chipSelectPin, LOW);
 
  //0XXXXXX0 formato de direcci�n
  SPI.transfer((addr<<1)&0x7E); 
  SPI.transfer(val);
 
  digitalWrite(_chipSelectPin, HIGH);
}
 
void RFID::antennaOn(void)
{
  unsigned char temp;
 
  temp = readMFRC522(TxControlReg);
  if (!(temp & 0x03))
  {
    setBitMask(TxControlReg, 0x03);
  }
}
 
/*
*  Read_MFRC522 Nombre de la funci�n: Read_MFRC522
*  Descripci�n: Desde el MFRC522 leer un byte de un registro de datos
*  Los par�metros de entrada: addr - la direcci�n de registro
*  Valor de retorno: Devuelve un byte de datos de lectura
*/
unsigned char RFID::readMFRC522(unsigned char addr)
{
  unsigned char val;
  digitalWrite(_chipSelectPin, LOW);
  SPI.transfer(((addr<<1)&0x7E) | 0x80);  
  val =SPI.transfer(0x00);
  digitalWrite(_chipSelectPin, HIGH);
  return val; 
}
 
void RFID::setBitMask(unsigned char reg, unsigned char mask)  
{ 
    unsigned char tmp;
    tmp = readMFRC522(reg);
    writeMFRC522(reg, tmp | mask);  // set bit mask
}
 
void RFID::clearBitMask(unsigned char reg, unsigned char mask)  
{
    unsigned char tmp;
    tmp = readMFRC522(reg);
    writeMFRC522(reg, tmp & (~mask));  // clear bit mask
} 
 
void RFID::calculateCRC(unsigned char *pIndata, unsigned char len, unsigned char *pOutData)
{
    unsigned char i, n;
 
    clearBitMask(DivIrqReg, 0x04);      //CRCIrq = 0
    setBitMask(FIFOLevelReg, 0x80);     //Claro puntero FIFO
    //Write_MFRC522(CommandReg, PCD_IDLE);
 
  //Escribir datos en el FIFO 
    for (i=0; i<len; i++)
    {   
    writeMFRC522(FIFODataReg, *(pIndata+i));   
  }
    writeMFRC522(CommandReg, PCD_CALCCRC);
 
  // Esperar a la finalizaci�n de c�lculo del CRC
    i = 0xFF;
    do 
    {
        n = readMFRC522(DivIrqReg);
        i--;
    }
    while ((i!=0) && !(n&0x04));      //CRCIrq = 1
 
  //Lea el c�lculo de CRC
    pOutData[0] = readMFRC522(CRCResultRegL);
    pOutData[1] = readMFRC522(CRCResultRegM);
}
 
unsigned char RFID::MFRC522ToCard(unsigned char command, unsigned char *sendData, unsigned char sendLen, unsigned char *backData, unsigned int *backLen)
{
    unsigned char status = MI_ERR;
    unsigned char irqEn = 0x00;
    unsigned char waitIRq = 0x00;
  unsigned char lastBits;
    unsigned char n;
    unsigned int i;
 
    switch (command)
    {
        case PCD_AUTHENT:   // Tarjetas de certificaci�n cerca
    {
      irqEn = 0x12;
      waitIRq = 0x10;
      break;
    }
    case PCD_TRANSCEIVE:  //La transmisi�n de datos FIFO
    {
      irqEn = 0x77;
      waitIRq = 0x30;
      break;
    }
    default:
      break;
    }
 
    writeMFRC522(CommIEnReg, irqEn|0x80); //De solicitud de interrupci�n
    clearBitMask(CommIrqReg, 0x80);     // Borrar todos los bits de petici�n de interrupci�n
    setBitMask(FIFOLevelReg, 0x80);     //FlushBuffer=1, FIFO de inicializaci�n
 
  writeMFRC522(CommandReg, PCD_IDLE); //NO action;Y cancelar el comando
 
  //Escribir datos en el FIFO
    for (i=0; i<sendLen; i++)
    {   
    writeMFRC522(FIFODataReg, sendData[i]);    
  }
 
  //???? ejecutar el comando
  writeMFRC522(CommandReg, command);
    if (command == PCD_TRANSCEIVE)
    {    
    setBitMask(BitFramingReg, 0x80);    //StartSend=1,transmission of data starts  
  }   
 
  // A la espera de recibir datos para completar
  i = 2000; //i????????,??M1???????25ms ??? i De acuerdo con el ajuste de frecuencia de reloj, el tiempo m�ximo de espera operaci�n M1 25ms tarjeta??
    do 
    {
    //CommIrqReg[7..0]
    //Set1 TxIRq RxIRq IdleIRq HiAlerIRq LoAlertIRq ErrIRq TimerIRq
        n = readMFRC522(CommIrqReg);
        i--;
    }
    while ((i!=0) && !(n&0x01) && !(n&waitIRq));
 
    clearBitMask(BitFramingReg, 0x80);      //StartSend=0
 
    if (i != 0)
    {    
        if(!(readMFRC522(ErrorReg) & 0x1B)) //BufferOvfl Collerr CRCErr ProtecolErr
        {
            status = MI_OK;
            if (n & irqEn & 0x01)
            {   
        status = MI_NOTAGERR;     //??   
      }
 
            if (command == PCD_TRANSCEIVE)
            {
                n = readMFRC522(FIFOLevelReg);
                lastBits = readMFRC522(ControlReg) & 0x07;
                if (lastBits)
                {   
          *backLen = (n-1)*8 + lastBits;   
        }
                else
                {   
          *backLen = n*8;   
        }
 
                if (n == 0)
                {   
          n = 1;    
        }
                if (n > MAX_LEN)
                {   
          n = MAX_LEN;   
        }
 
        //??FIFO??????? Lea los datos recibidos en el FIFO
                for (i=0; i<n; i++)
                {   
          backData[i] = readMFRC522(FIFODataReg);    
        }
            }
        }
        else
        {   
      status = MI_ERR;  
    }
 
    }
 
    //SetBitMask(ControlReg,0x80);           //timer stops
    //Write_MFRC522(CommandReg, PCD_IDLE); 
 
    return status;
}
 
 
/*
*  Nombre de la funci�n: MFRC522_Request
*  Descripci�n: Buscar las cartas, leer el n�mero de tipo de tarjeta
*  Los par�metros de entrada: reqMode - encontrar el modo de tarjeta,
*         Tagtype - Devuelve el tipo de tarjeta
*        0x4400 = Mifare_UltraLight
*        0x0400 = Mifare_One(S50)
*        0x0200 = Mifare_One(S70)
*        0x0800 = Mifare_Pro(X)
*        0x4403 = Mifare_DESFire
*  Valor de retorno: el retorno exitoso MI_OK
*/
unsigned char  RFID::MFRC522Request(unsigned char reqMode, unsigned char *TagType)
{
  unsigned char status;  
  unsigned int backBits;      //   Recibi� bits de datos
 
  writeMFRC522(BitFramingReg, 0x07);    //TxLastBists = BitFramingReg[2..0] ???
 
  TagType[0] = reqMode;
  status = MFRC522ToCard(PCD_TRANSCEIVE, TagType, 1, TagType, &backBits);
 
  if ((status != MI_OK) || (backBits != 0x10))
  {    
    status = MI_ERR;
  }
 
  return status;
}
 
/**
*  MFRC522Anticoll -> anticoll
*  Anti-detecci�n de colisiones, la lectura del n�mero de serie de la tarjeta de tarjeta
*  @param serNum - devuelve el n�mero de tarjeta 4 bytes de serie, los primeros 5 bytes de bytes de paridad
*  @return retorno exitoso MI_OK
*/
unsigned char RFID::anticoll(unsigned char *serNum)
{
    unsigned char status;
    unsigned char i;
  unsigned char serNumCheck=0;
    unsigned int unLen;
 
 
    //ClearBitMask(Status2Reg, 0x08);   //TempSensclear
    //ClearBitMask(CollReg,0x80);     //ValuesAfterColl
  writeMFRC522(BitFramingReg, 0x00);    //TxLastBists = BitFramingReg[2..0]
 
    serNum[0] = PICC_ANTICOLL;
    serNum[1] = 0x20;
    status = MFRC522ToCard(PCD_TRANSCEIVE, serNum, 2, serNum, &unLen);
 
    if (status == MI_OK)
  {
    //?????? Compruebe el n�mero de serie de la tarjeta
    for (i=0; i<4; i++)
    {   
      serNumCheck ^= serNum[i];
    }
    if (serNumCheck != serNum[i])
    {   
      status = MI_ERR;    
    }
    }
 
    //SetBitMask(CollReg, 0x80);    //ValuesAfterColl=1
 
    return status;
}
 
/* 
* MFRC522Auth -> auth
* Verificar la contrase�a de la tarjeta
* Los par�metros de entrada: AuthMode - Modo de autenticaci�n de contrase�a
                 0x60 = A 0x60 = validaci�n KeyA
                 0x61 = B 0x61 = validaci�n KeyB
             BlockAddr--  bloque de direcciones
             Sectorkey-- sector contrase�a
             serNum--,4? Tarjeta de n�mero de serie, 4 bytes
* MI_OK Valor de retorno: el retorno exitoso MI_OK
*/
unsigned char RFID::auth(unsigned char authMode, unsigned char BlockAddr, unsigned char *Sectorkey, unsigned char *serNum)
{
    unsigned char status;
    unsigned int recvBits;
    unsigned char i;
  unsigned char buff[12]; 
 
  //????+???+????+???? Verifique la direcci�n de comandos de bloques del sector + + contrase�a + n�mero de la tarjeta de serie
    buff[0] = authMode;
    buff[1] = BlockAddr;
    for (i=0; i<6; i++)
    {    
    buff[i+2] = *(Sectorkey+i);   
  }
    for (i=0; i<4; i++)
    {    
    buff[i+8] = *(serNum+i);   
  }
    status = MFRC522ToCard(PCD_AUTHENT, buff, 12, buff, &recvBits);
 
    if ((status != MI_OK) || (!(readMFRC522(Status2Reg) & 0x08)))
    {   
    status = MI_ERR;   
  }
 
    return status;
}
 
/*
* MFRC522Read -> read
* Lectura de datos de bloque
* Los par�metros de entrada: blockAddr - direcci�n del bloque; recvData - leer un bloque de datos
* MI_OK Valor de retorno: el retorno exitoso MI_OK
*/
unsigned char RFID::read(unsigned char blockAddr, unsigned char *recvData)
{
    unsigned char status;
    unsigned int unLen;
 
    recvData[0] = PICC_READ;
    recvData[1] = blockAddr;
    calculateCRC(recvData,2, &recvData[2]);
    status = MFRC522ToCard(PCD_TRANSCEIVE, recvData, 4, recvData, &unLen);
 
    if ((status != MI_OK) || (unLen != 0x90))
    {
        status = MI_ERR;
    }
 
    return status;
}
 
/*
* MFRC522Write -> write
* La escritura de datos de bloque
* blockAddr - direcci�n del bloque; WriteData - para escribir 16 bytes del bloque de datos
* Valor de retorno: el retorno exitoso MI_OK
*/
unsigned char RFID::write(unsigned char blockAddr, unsigned char *writeData)
{
    unsigned char status;
    unsigned int recvBits;
    unsigned char i;
  unsigned char buff[18]; 
 
    buff[0] = PICC_WRITE;
    buff[1] = blockAddr;
    calculateCRC(buff, 2, &buff[2]);
    status = MFRC522ToCard(PCD_TRANSCEIVE, buff, 4, buff, &recvBits);
 
    if ((status != MI_OK) || (recvBits != 4) || ((buff[0] & 0x0F) != 0x0A))
    {   
    status = MI_ERR;   
  }
 
    if (status == MI_OK)
    {
        for (i=0; i<16; i++)    //?FIFO?16Byte?? Datos a la FIFO 16Byte escribir
        {    
          buff[i] = *(writeData+i);   
        }
        calculateCRC(buff, 16, &buff[16]);
        status = MFRC522ToCard(PCD_TRANSCEIVE, buff, 18, buff, &recvBits);
 
    if ((status != MI_OK) || (recvBits != 4) || ((buff[0] & 0x0F) != 0x0A))
        {   
      status = MI_ERR;   
    }
    }
 
    return status;
}
 
 
/*
* MFRC522Halt -> halt
* Cartas de Mando para dormir
* Los par�metros de entrada: Ninguno
* Valor devuelto: Ninguno
*/
void RFID::halt()
{
    unsigned char status;
    unsigned int unLen;
    unsigned char buff[4];
 
    buff[0] = PICC_HALT;
    buff[1] = 0;
    calculateCRC(buff, 2, &buff[2]);
 
    clearBitMask(Status2Reg, 0x08); // turn off encryption
 
    status = MFRC522ToCard(PCD_TRANSCEIVE, buff, 4, buff,&unLen);
}






 
/*
  Arduino.h - Main include file for the Arduino SDK
  Copyright (c) 2005-2013 Arduino Team.  All right reserved.
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
 
#ifndef Arduino_h
#define Arduino_h
 
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
 
#include <avr/pgmspace.h>
#include <avr/io.h>
#include <avr/interrupt.h>
 
#include "binary.h"
 
#ifdef __cplusplus
extern "C"{
#endif
 
void yield(void);
 
#define HIGH 0x1
#define LOW  0x0
 
#define INPUT 0x0
#define OUTPUT 0x1
#define INPUT_PULLUP 0x2
 
#define PI 3.1415926535897932384626433832795
#define HALF_PI 1.5707963267948966192313216916398
#define TWO_PI 6.283185307179586476925286766559
#define DEG_TO_RAD 0.017453292519943295769236907684886
#define RAD_TO_DEG 57.295779513082320876798154814105
#define EULER 2.718281828459045235360287471352
 
#define SERIAL  0x0
#define DISPLAY 0x1
 
#define LSBFIRST 0
#define MSBFIRST 1
 
#define CHANGE 1
#define FALLING 2
#define RISING 3
 
#if defined(__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
  #define DEFAULT 0
  #define EXTERNAL 1
  #define INTERNAL1V1 2
  #define INTERNAL INTERNAL1V1
#elif defined(__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
  #define DEFAULT 0
  #define EXTERNAL 4
  #define INTERNAL1V1 8
  #define INTERNAL INTERNAL1V1
  #define INTERNAL2V56 9
  #define INTERNAL2V56_EXTCAP 13
#else  
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__) || defined(__AVR_ATmega1284__) || defined(__AVR_ATmega1284P__) || defined(__AVR_ATmega644__) || defined(__AVR_ATmega644A__) || defined(__AVR_ATmega644P__) || defined(__AVR_ATmega644PA__)
#define INTERNAL1V1 2
#define INTERNAL2V56 3
#else
#define INTERNAL 3
#endif
#define DEFAULT 1
#define EXTERNAL 0
#endif
 
// undefine stdlib's abs if encountered
#ifdef abs
#undef abs
#endif
 
#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#define abs(x) ((x)>0?(x):-(x))
#define constrain(amt,low,high) ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))
#define round(x)     ((x)>=0?(long)((x)+0.5):(long)((x)-0.5))
#define radians(deg) ((deg)*DEG_TO_RAD)
#define degrees(rad) ((rad)*RAD_TO_DEG)
#define sq(x) ((x)*(x))
 
#define interrupts() sei()
#define noInterrupts() cli()
 
#define clockCyclesPerMicrosecond() ( F_CPU / 1000000L )
#define clockCyclesToMicroseconds(a) ( (a) / clockCyclesPerMicrosecond() )
#define microsecondsToClockCycles(a) ( (a) * clockCyclesPerMicrosecond() )
 
#define lowByte(w) ((uint8_t) ((w) & 0xff))
#define highByte(w) ((uint8_t) ((w) >> 8))
 
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define bitSet(value, bit) ((value) |= (1UL << (bit)))
#define bitClear(value, bit) ((value) &= ~(1UL << (bit)))
#define bitWrite(value, bit, bitvalue) (bitvalue ? bitSet(value, bit) : bitClear(value, bit))
 
// avr-libc defines _NOP() since 1.6.2
#ifndef _NOP
#define _NOP() do { __asm__ volatile ("nop"); } while (0)
#endif
 
typedef unsigned int word;
 
#define bit(b) (1UL << (b))
 
typedef bool boolean;
typedef uint8_t byte;
 
void init(void);
void initVariant(void);
 
int atexit(void (*func)()) __attribute__((weak));
 
void pinMode(uint8_t, uint8_t);
void digitalWrite(uint8_t, uint8_t);
int digitalRead(uint8_t);
int analogRead(uint8_t);
void analogReference(uint8_t mode);
void analogWrite(uint8_t, int);
 
unsigned long millis(void);
unsigned long micros(void);
void delay(unsigned long);
void delayMicroseconds(unsigned int us);
unsigned long pulseIn(uint8_t pin, uint8_t state, unsigned long timeout);
unsigned long pulseInLong(uint8_t pin, uint8_t state, unsigned long timeout);
 
void shiftOut(uint8_t dataPin, uint8_t clockPin, uint8_t bitOrder, uint8_t val);
uint8_t shiftIn(uint8_t dataPin, uint8_t clockPin, uint8_t bitOrder);
 
void attachInterrupt(uint8_t, void (*)(void), int mode);
void detachInterrupt(uint8_t);
 
void setup(void);
void loop(void);
 
// Get the bit location within the hardware port of the given virtual pin.
// This comes from the pins_*.c file for the active board configuration.
 
#define analogInPinToBit(P) (P)
 
// On the ATmega1280, the addresses of some of the port registers are
// greater than 255, so we can't store them in uint8_t's.
extern const uint16_t PROGMEM port_to_mode_PGM[];
extern const uint16_t PROGMEM port_to_input_PGM[];
extern const uint16_t PROGMEM port_to_output_PGM[];
 
extern const uint8_t PROGMEM digital_pin_to_port_PGM[];
// extern const uint8_t PROGMEM digital_pin_to_bit_PGM[];
extern const uint8_t PROGMEM digital_pin_to_bit_mask_PGM[];
extern const uint8_t PROGMEM digital_pin_to_timer_PGM[];
 
// Get the bit location within the hardware port of the given virtual pin.
// This comes from the pins_*.c file for the active board configuration.
// 
// These perform slightly better as macros compared to inline functions
//
#define digitalPinToPort(P) ( pgm_read_byte( digital_pin_to_port_PGM + (P) ) )
#define digitalPinToBitMask(P) ( pgm_read_byte( digital_pin_to_bit_mask_PGM + (P) ) )
#define digitalPinToTimer(P) ( pgm_read_byte( digital_pin_to_timer_PGM + (P) ) )
#define analogInPinToBit(P) (P)
#define portOutputRegister(P) ( (volatile uint8_t *)( pgm_read_word( port_to_output_PGM + (P))) )
#define portInputRegister(P) ( (volatile uint8_t *)( pgm_read_word( port_to_input_PGM + (P))) )
#define portModeRegister(P) ( (volatile uint8_t *)( pgm_read_word( port_to_mode_PGM + (P))) )
 
#define NOT_A_PIN 0
#define NOT_A_PORT 0
 
#define NOT_AN_INTERRUPT -1
 
#ifdef ARDUINO_MAIN
#define PA 1
#define PB 2
#define PC 3
#define PD 4
#define PE 5
#define PF 6
#define PG 7
#define PH 8
#define PJ 10
#define PK 11
#define PL 12
#endif
 
#define NOT_ON_TIMER 0
#define TIMER0A 1
#define TIMER0B 2
#define TIMER1A 3
#define TIMER1B 4
#define TIMER1C 5
#define TIMER2  6
#define TIMER2A 7
#define TIMER2B 8
 
#define TIMER3A 9
#define TIMER3B 10
#define TIMER3C 11
#define TIMER4A 12
#define TIMER4B 13
#define TIMER4C 14
#define TIMER4D 15
#define TIMER5A 16
#define TIMER5B 17
#define TIMER5C 18
 
#ifdef __cplusplus
} // extern "C"
#endif
 
#ifdef __cplusplus
#include "WCharacter.h"
#include "WString.h"
#include "HardwareSerial.h"
#include "USBAPI.h"
#if defined(HAVE_HWSERIAL0) && defined(HAVE_CDCSERIAL)
#error "Targets with both UART0 and CDC serial not supported"
#endif
 
uint16_t makeWord(uint16_t w);
uint16_t makeWord(byte h, byte l);
 
#define word(...) makeWord(__VA_ARGS__)
 
unsigned long pulseIn(uint8_t pin, uint8_t state, unsigned long timeout = 1000000L);
unsigned long pulseInLong(uint8_t pin, uint8_t state, unsigned long timeout = 1000000L);
 
void tone(uint8_t _pin, unsigned int frequency, unsigned long duration = 0);
void noTone(uint8_t _pin);
 
// WMath prototypes
long random(long);
long random(long, long);
void randomSeed(unsigned long);
long map(long, long, long, long, long);
 
#endif
 
#include "pins_arduino.h"
 
#endif
