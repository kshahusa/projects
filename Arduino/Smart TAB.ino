#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <SPI.h>       
 
const int buttonPin = 2;
int buttonState = 0;
 
const int sensorPin = A0;
    
#define DS3231_I2C_ADDRESS 0x68   
    
byte decToBcd(byte val)   
{   
  return( (val/10*16) + (val%10) );   
}   
    
byte bcdToDec(byte val)   
{   
  return( (val/16*10) + (val%16) );   
}
 
#if defined(ARDUINO) && ARDUINO >= 100
#define printByte(args)  write(args);
#else
#define printByte(args)  print(args,BYTE);
#endif
 
uint8_t bell[7]  = {0x4,0xe,0xe,0xe,0x1f,0x0,0x4};
uint8_t note[8]  = {0x2,0x3,0x2,0xe,0x1e,0xc,0x0};
uint8_t clock[7] = {0x0,0xe,0x15,0x17,0x11,0xe,0x0};
uint8_t heart[8] = {0x0,0xa,0x1f,0x1f,0xe,0x4,0x0};
uint8_t duck[8]  = {0x0,0xc,0x1d,0xf,0xf,0x6,0x0};
uint8_t check[8] = {0x0,0x1,0x3,0x16,0x1c,0x8,0x0};
uint8_t cross[8] = {0x0,0x1b,0xe,0x4,0xe,0x1b,0x0};
uint8_t retarrow[8] = {0x1,0x1,0x5,0x9,0x1f,0x8,0x4};
uint8_t pound[8] = {0x7,0x8,0x8,0x1e,0x8,0x8,0x1f}; 
uint8_t euro[8] = {0x3,0x4,0x8,0x1f,0x8,0x1f,0x4,0x3};
uint8_t degreec[8] = {0x8,0xf4,0x8,0x43,0x4,0x4,0x43,0x0};
uint8_t degreef[8] = {0x0B,0x16,0x0A,0x07,0x02,0x02,0x02,0x02};
uint8_t degree[8] = {0x08,0x14,0x08,0x00,0x00,0x00,0x00,0x00};
uint8_t dollar[8] = {0x0E,0x0B,0x1A,0x0E,0x0A,0x0B,0x1A,0x0E};
uint8_t rupee[8] = {0x1F,0x08,0x1F,0x08,0x04,0x02,0x01,0x00};
  
LiquidCrystal_I2C lcd(0x3F,16,2);  
 
void setup()
{
 
  pinMode(buttonPin, INPUT_PULLUP);
 
  lcd.init();                       
  lcd.backlight();
  
  lcd.createChar(0, bell);
  lcd.createChar(1, note);
  lcd.createChar(2, clock);
  lcd.createChar(3, heart);
  lcd.createChar(4, duck);
  lcd.createChar(5, check);
  lcd.createChar(6, cross);
  lcd.createChar(7, retarrow);
  lcd.createChar(8, pound);
  lcd.createChar(9, euro);
  lcd.createChar(10, degreec);
  lcd.createChar(11, degreef);
  lcd.createChar(12, degree);
  lcd.createChar(13, dollar);
  lcd.createChar(14, rupee);
  lcd.home();
 
  lcd.print("Smart TAB Says:");
  lcd.setCursor(0,1);
  lcd.print("Jay Swaminarayan");
  delay(2000);
  lcd.clear();
  lcd.home();
  lcd.print("Loading...");
  delay(3000);
                          
  Wire.begin();   
  Serial.begin(9600);   
  // set the initial time here:   
  // DS3231 seconds, minutes, hours, day, date, month, year   
  //setDS3231time(0,31,16,2,9,10,17);  
     
}   
void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte   
dayOfMonth, byte month, byte year)   
{   
  // sets time and date data to DS3231   
  Wire.beginTransmission(DS3231_I2C_ADDRESS);   
  Wire.write(0); // set next input to start at the seconds register   
  Wire.write(decToBcd(second)); // set seconds   
  Wire.write(decToBcd(minute)); // set minutes   
  Wire.write(decToBcd(hour)); // set hours   
  Wire.write(decToBcd(dayOfWeek)); // set day of week (1=Sunday, 7=Saturday)   
  Wire.write(decToBcd(dayOfMonth)); // set date (1 to 31)   
  Wire.write(decToBcd(month)); // set month   
  Wire.write(decToBcd(year)); // set year (0 to 99)   
  Wire.endTransmission();   
}   
void readDS3231time(byte *second,   
byte *minute,   
byte *hour,   
byte *dayOfWeek,   
byte *dayOfMonth,   
byte *month,   
byte *year)   
{   
  Wire.beginTransmission(DS3231_I2C_ADDRESS);   
  Wire.write(0); // set DS3231 register pointer to 00h   
  Wire.endTransmission();   
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);   
  // request seven bytes of data from DS3231 starting from register 00h   
  *second = bcdToDec(Wire.read() & 0x7f);   
  *minute = bcdToDec(Wire.read());   
  *hour = bcdToDec(Wire.read() & 0x3f);   
  *dayOfWeek = bcdToDec(Wire.read());   
  *dayOfMonth = bcdToDec(Wire.read());   
  *month = bcdToDec(Wire.read());   
  *year = bcdToDec(Wire.read());   
}   
void displayTime()   
{   
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;   
  // retrieve data from DS3231   
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month,   
  &year);   
  // send it to the serial monitor   
  lcd.print(hour, DEC);   
  // convert the byte variable to a decimal number when displayed   
  lcd.print(":");   
  if (minute<10)   
  {   
    lcd.print("0");   
  }   
  lcd.print(minute, DEC);      
  lcd.print(":");
  if (second<10)   
  {   
    lcd.print("0");   
  }
  lcd.print(second, DEC);     
  lcd.print(" ");   
  lcd.print(dayOfMonth, DEC);   
  lcd.print("/");   
  lcd.print(month, DEC);   
  lcd.print("/");   
  lcd.print(year, DEC);   
  lcd.setCursor(0,1);   
  switch(dayOfWeek){   
  case 1:   
    lcd.print("Sunday");
    break;   
  case 2:   
    lcd.print("Monday");
    break;   
  case 3:   
    lcd.print("Tuesday");
    break;   
  case 4:   
    lcd.print("Wednesday");
    break;   
  case 5:   
    lcd.print("Thursday");   
    break;   
  case 6:   
    lcd.print("Friday");
    break;   
  case 7:   
    lcd.print("Saturday");   
    break;   
  } 
  delay(1000);
  lcd.clear(); 
}   
 
void loop()
{
 
buttonState = digitalRead(buttonPin);
Serial.println(buttonState);
 
if(buttonState == HIGH){
 
displayTime();
 
}
 
else{
 
lcd.clear();
int reading = analogRead(sensorPin);   
  
float voltage = reading * 5.0; 
voltage /= 1024.0;
 
float temperatureC = (0.5 - voltage) * 100. ;    
  
float temperatureF = (temperatureC * 9.0 / 5.0) + 32.0; 
lcd.print(temperatureF); 
lcd.printByte(11);
lcd.print(" ");
lcd.print(temperatureC);
lcd.printByte(10);
delay(1000);
 
} 
}
/*
 
https://facelesstech.wordpress.com/2014/07/29/custom-characters-on-lcd-1602-with-i2c-interface/
 
0x1f xxxxx
0xf  0xxxx
0x7  00xxx
0x3  000xx
0x21 0000x
0xf0 x0000
0xf8 xx000
0x1c xxx00
0x1e xxxx0
 
0x8  0x000
0x4  00x00
0x2  000x0
0x1  0000x
 
0x38 xx000
0xc  0xx00
0x6  00xx0
0x43 000xx
 
0x0  00000
 
0x1d xxx0x
0x17 x0xxx
 
0xe  0xxx0
0x1b xx0xx
0x11 x000x
0x15 x0x0x
0xa  0x0x0
 
0x19 xx00x
0x3a xx0x0
0xd  0xx0x
0x16 x0xx0
0xf3 x00xx
 
0xf2 x00x0
0x9  0x00x
0xa  0x0x0
0x5  00x0x
0xf4 x0x00
*/
