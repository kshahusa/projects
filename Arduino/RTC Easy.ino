USE DS3231 LIBRARY FOR FULL FUNCTION

#include <DS3231.h>
 
DS3231  rtc(SDA, SCL);
Time  t;
 
void rtc_hardtime() {
t = rtc.getTime();
 
  Serial.print("Today is the ");
  Serial.print(t.date, DEC);
  Serial.print(". day of ");
  Serial.print(rtc.getMonthStr());
  Serial.print(" in the year ");
  Serial.print(t.year, DEC);
  Serial.println(".");
 
  Serial.print("It is the ");
  Serial.print(t.dow, DEC);
  Serial.print(". day of the week (counting monday as the 1th), and it has passed ");
  Serial.print(t.hour-1', DEC);
  Serial.print(" hour(s), ");
  Serial.print(t.min, DEC);
  Serial.print(" minute(s) and ");
  Serial.print(t.sec, DEC);
  Serial.println(" second(s) since midnight.");
 
  Serial.println("  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -");
 
  delay (1000);
}
 
void rtc_gettemp() {
  Serial.print("Temperature: ");
  Serial.print(rtc.getTemp());
  Serial.println(" C");
  delay (1000);
}
 
void setup() {
  
Serial.begin(9600);
rtc.begin();
 
}
 
void loop() {
  // put your main code here, to run repeatedly:
 
}
