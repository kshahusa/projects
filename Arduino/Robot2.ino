#include <Servo.h>
#include <Stepper.h> 
 
const int stepsPerRevolution = 100;  
 
Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11); 
Servo myServo;
 
const int buzzer=6;
 
const int servoPin=7; 
const int servoPosition=150; 
 
int trgPin=13; 
int echoPin=12; 
 
float pingTime; 
float speedOfSound=739.50; 
float targetDistance;
 
int stepCount = 0;  
int sensorPin = A0;
 
int ledPin = 13; 
int sensorValue = 0; 
 
void setup () {
 
  Serial.begin (9600);
 
  pinMode(ledPin, OUTPUT);
  pinMode(sensorPin,INPUT);
 
  pinMode(trgPin,OUTPUT); 
  pinMode(echoPin,INPUT);
 
  pinMode(buzzer,OUTPUT);
  
}
 
void loop () {
 
  digitalWrite(trgPin,LOW); 
  delayMicroseconds (2000); 
  digitalWrite(trgPin,HIGH); 
  delayMicroseconds (10); 
  digitalWrite(trgPin,LOW); 
 
  pingTime=pulseIn(echoPin,HIGH); 
  pingTime=pingTime/1000000.; 
  pingTime=pingTime/3600.; 
  targetDistance=speedOfSound*pingTime; 
  targetDistance=targetDistance/2; 
  targetDistance=targetDistance*63360;
 
  sensorValue = analogRead (sensorPin);
  digitalWrite (ledPin, HIGH);
  delay (sensorValue);
  digitalWrite (ledPin, LOW);
  delay (sensorValue);
  Serial.println (sensorValue, DEC);//26
 
if (sensorValue<=26) {
  analogWrite(buzzer,255);
  delay(500);
  analogWrite(buzzer,100);
  delay(500);
  analogWrite(buzzer,255);
  delay(500);
  analogWrite(buzzer,200);
  delay(500);
}
 
  int sensorReading = analogRead(A0);  
  int motorSpeed = map(sensorReading, 0, 1023, 0, 100); 
 
if (motorSpeed > 0) { 
    myStepper.setSpeed(motorSpeed);  
   myStepper.step(stepsPerRevolution / 100); 
}
 
if (targetDistance<=2) {
  
  myServo.write(servoPosition);
 
}  
}
