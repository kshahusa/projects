#include <NewPing.h> 
#include <LCD.h> 
#include <LiquidCrystal_I2C.h>    
#include <Wire.h>          
#include <SPI.h>  
 
#if defined(ARDUINO) && ARDUINO >= 100
#define printByte(args)  write(args);
#else
#define printByte(args)  print(args,BYTE);
#endif
 
uint8_t bell[8]  = {0x4,0xe,0xe,0xe,0x1f,0x0,0x4};
uint8_t note[8]  = {0x2,0x3,0x2,0xe,0x1e,0xc,0x0};
uint8_t clock[8] = {0x0,0xe,0x15,0x17,0x11,0xe,0x0};
uint8_t heart[8] = {0x0,0xa,0x1f,0x1f,0xe,0x4,0x0};
uint8_t duck[8]  = {0x0,0xc,0x1d,0xf,0xf,0x6,0x0};
uint8_t check[8] = {0x0,0x1,0x3,0x16,0x1c,0x8,0x0};
uint8_t cross[8] = {0x0,0x1b,0xe,0x4,0xe,0x1b,0x0};
uint8_t retarrow[8] = {  0x1,0x1,0x5,0x9,0x1f,0x8,0x4};
 
#define TRIGGER_PIN  10   
#define ECHO_PIN     9   
#define MAX_DISTANCE 200  
  
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);   
 
#define I2C_ADDR    0x3F  
#define BACKLIGHT_PIN     3 
#define En_pin  2 
#define Rw_pin  1 
#define Rs_pin  0 
#define D4_pin  4 
#define D5_pin  5 
#define D6_pin  6 
#define D7_pin  7 
  
int n = 1; 
  
LiquidCrystal_I2C  LCD(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin);
 
#define DS3231_I2C_ADDRESS 0x68      
       
byte decToBcd(byte val)      
{      
  return( (val/10*16) + (val%10) );      
}      
       
byte bcdToDec(byte val)      
{      
  return( (val/16*10) + (val%16) );      
}      
         
void setup() {     
   
Serial.begin(9600);  
 
LCD.setBacklightPin(BACKLIGHT_PIN,POSITIVE); 
LCD.setBacklight(HIGH);
         
  LCD.begin(16,2);    
  LCD.setCursor(0,0);  
  LCD.print("Welcome to:");  
  LCD.setCursor(0,1);  
  LCD.print("The Safe Bike!");  
  delay(4000);    
  LCD.createChar(0, bell);
  LCD.createChar(1, note);
  LCD.createChar(2, clock);
  LCD.createChar(3, heart);
  LCD.createChar(4, duck);
  LCD.createChar(5, check);
  LCD.createChar(6, cross);
  LCD.createChar(7, retarrow);
 
  LCD.clear();  
  LCD.home();
  LCD.print("Loading...");
  LCD.printByte(1);
  LCD.printByte(2);
  LCD.printByte(5);
  LCD.printByte(6);
  LCD.printByte(7);
  delay(3000);
  LCD.clear();  
   
  Wire.begin();        
  // set the initial time here:      
  // DS3231 seconds, minutes, hours, day, date, month, year      
  //setDS3231time(0,57,17,2,19,9,17);     
        
}      
void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte      
dayOfMonth, byte month, byte year)      
{      
  // sets time and date data to DS3231      
  Wire.beginTransmission(DS3231_I2C_ADDRESS);      
  Wire.write(0);       
  Wire.write(decToBcd(second));       
  Wire.write(decToBcd(minute));     
  Wire.write(decToBcd(hour));       
  Wire.write(decToBcd(dayOfWeek));   
  Wire.write(decToBcd(dayOfMonth));       
  Wire.write(decToBcd(month));       
  Wire.write(decToBcd(year));      
  Wire.endTransmission();      
}      
void readDS3231time(byte *second,      
byte *minute,      
byte *hour,      
byte *dayOfWeek,      
byte *dayOfMonth,      
byte *month,      
byte *year)      
{      
  Wire.beginTransmission(DS3231_I2C_ADDRESS);      
  Wire.write(0);    
  Wire.endTransmission();      
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);      
   
  *second = bcdToDec(Wire.read() & 0x7f);      
  *minute = bcdToDec(Wire.read());      
  *hour = bcdToDec(Wire.read() & 0x3f);      
  *dayOfWeek = bcdToDec(Wire.read());      
  *dayOfMonth = bcdToDec(Wire.read());      
  *month = bcdToDec(Wire.read());      
  *year = bcdToDec(Wire.read());      
}      
void displayTime()      
{      
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;      
       
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month,      
  &year);      
       
  LCD.print(hour, DEC);      
       
  LCD.print(":");      
  if (minute<10)      
  {      
    LCD.print("0");      
  }      
  LCD.print(minute, DEC);    
  LCD.print(":");      
if (second<10)      
  {      
    LCD.print("0");      
  }      
  LCD.print(second, DEC);        
  LCD.print(" ");      
  LCD.print(dayOfMonth, DEC);      
  LCD.print("/");      
  LCD.print(month, DEC);      
  LCD.print("/");      
  LCD.print(year, DEC);      
  LCD.setCursor(0,1);    
  switch(dayOfWeek){      
  case 1:      
    LCD.println("Sunday");      
    break;      
  case 2:      
    LCD.println("Monday");      
    break;      
  case 3:      
    LCD.println("Tuesday");      
    break;      
  case 4:      
    LCD.println("Wednesday");      
    break;      
  case 5:      
    LCD.println("Thursday");      
    break;      
  case 6:      
    LCD.println("Friday");      
    break;      
  case 7:      
    LCD.println("Saturday");      
    break;      
  } 
}        
         
void loop() {           
 
  LCD.leftToRight();
  LCD.noAutoscroll(); 
  delay(50); 
  LCD.print(" "); 
  //LCD.print("Ping: "); 
  LCD.print(sonar.ping_cm()); 
  LCD.print("cm."); 
  delay(1500); 
  LCD.setCursor(0,0);    
  displayTime();        
  LCD.print(" ");  
  
}       
