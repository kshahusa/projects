#include <Servo.h>
int pos=0;
int servoPin=9;
int servoDelay=25;
int joyReading;
int joyPin=A0;
int buzzer=2;
Servo myServo;

void setup() {

  Serial.begin(9600);
  myServo.attach(servoPin);
  pinMode(buzzer,OUTPUT);

}

void loop() {
 joyReading = analogRead(joyPin);
 pos = (155./1023.)*joyReading+15;
 myServo.write(pos);

  digitalWrite(buzzer,HIGH);
  delay(100);
  digitalWrite(buzzer,LOW);
  delay(100);
}

