 
const int potPin = A0;
const int motor = 6;
int readVal = 0;
 
void setup() {
  
  Serial.begin(9600);
  pinMode(motor,OUTPUT);
 
}
 
void loop() {
  
readVal = analogRead(potPin);
  analogWrite(motor,readVal);
 
  Serial.println(readVal);
  delay(500);
  readVal = 0;
  analogWrite(motor,readVal);
  delay(1000);
 
}



#include <Servo.h> 
 
Servo myServo;
 
int pos=0; 
const int servoPin=9; 
const int servoDelay=25; 
 
int VRxReading; 
const int VRx=A0;
int VRyReading; 
const int VRy=A1;
int swReading; 
const int sw=A2;   
 
void setup() {  
 
  Serial.begin(9600); 
  myServo.attach(servoPin);   
 
} 
 
 
void loop() { 
 
VRxReading = analogRead(VRx);
VRyReading = analogRead(VRy);
swReading = analogRead(sw); 
 
if(VRxReading){
pos = (155./1023.)*VRyReading+15; 
myServo.write(pos);
}
}  
