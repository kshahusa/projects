int trgPin=13;
int echoPin=11;
float pingTime;
float speedOfSound;
float targetDistance;
String password;
int redPin=2;
int greenPin=3;
int bluePin=4;
int potPen=A0;
int Krishna;

void setup() {
  Serial.begin(9600);
  pinMode(trgPin,OUTPUT);
  pinMode(echoPin,INPUT);
  Serial.println("");  
  Serial.println("Coded and Debuged");
  Serial.println("by Krishna");
  Serial.println("");   
  if (potPen=255) {
    analogWrite (greenPin,0);
    analogWrite (bluePin,255);
    analogWrite (redPin,0);    
  }
  Serial.println("Please enter your password to continue");
  Serial.println("Otherwise ask user for password");
  while (Serial.available()==0) {}
  if (password=Krishna) {
    analogWrite (greenPin,255);
    analogWrite (bluePin,0);
    analogWrite (redPin,0);    
    }
  if (password!=Krishna) {
    analogWrite (greenPin,0);
    analogWrite (bluePin,0);
    analogWrite (redPin,255);
  }
}

void loop() {
    
    analogWrite (greenPin,0);
    analogWrite (bluePin,0);
    analogWrite (redPin,0);
    
    analogWrite (greenPin,0);
    analogWrite (bluePin,255);
    analogWrite (redPin,0);
  
  digitalWrite(trgPin,LOW);
  delayMicroseconds (2000);
  digitalWrite(trgPin,HIGH);
  delayMicroseconds (10);
  digitalWrite(trgPin,LOW);

  pingTime=pulseIn(echoPin,HIGH);

  speedOfSound= 2*targetDistance/pingTime;
  speedOfSound=speedOfSound/63360*1000000*3600;
  Serial.print("The speed of sound is: ");
  Serial.print(speedOfSound);
  delay (1000);
  Serial.println(" miles per hour");
  Serial.println("");

}

