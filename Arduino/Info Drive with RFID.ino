
#include <LiquidCrystal.h>    
#include <Wire.h>          
#include <SPI.h>
#include <AddicoreRFID.h>      

#define uchar unsigned char 
#define uint unsigned int 
 
 
uchar fifobytes; 
uchar fifoValue; 
 
 
AddicoreRFID myRFID; // create AddicoreRFID object to control the RFID module 
 
 
///////////////////////////////////////////////////////////////////// 
//set the pins 
///////////////////////////////////////////////////////////////////// 
const int chipSelectPin = 10; 
const int NRSTPD = 5; 
 
 
//Maximum length of the array 
#define MAX_LEN 16 
   
const int joy_sw=A1;  

LiquidCrystal LCD(7,6,5,4,3,2);    
     
const int sensorPin = A0;      
       
#define DS3231_I2C_ADDRESS 0x68      
       
byte decToBcd(byte val)      
{      
  return( (val/10*16) + (val%10) );      
}      
       
byte bcdToDec(byte val)      
{      
  return( (val/16*10) + (val%16) );      
}      
         
void setup() {     
   
Serial.begin(9600);  

  LCD.begin(16,2);    
  LCD.setCursor(0,0); 

  // start the SPI library: 
  SPI.begin(); 
   
  pinMode(chipSelectPin,OUTPUT);              // Set digital pin 10 as OUTPUT to connect it to the RFID /ENABLE pin  
    digitalWrite(chipSelectPin, LOW);         // Activate the RFID reader 
  pinMode(NRSTPD,OUTPUT);                     // Set digital pin 10 , Not Reset and Power-down 
    digitalWrite(NRSTPD, HIGH);

  uchar i, tmp, checksum1; 
uchar status; 
        uchar str[MAX_LEN]; 
        uchar RC_size; 
        uchar blockAddr; //Selection operation block address 0 to 63 
        String mynum = ""; 
 
 
        str[1] = 0x4400; 
//Find tags, return tag type 
status = myRFID.AddicoreRFID_Request(PICC_REQIDL, str);  
if (status == MI_OK) 
{ 
          Serial.println("RFID tag detected"); 
          Serial.print("Tag Type:\t\t"); 
          uint tagType = str[0] << 8; 
          tagType = tagType + str[1]; 
          switch (tagType) { 
            case 0x4400: 
              Serial.println("Mifare UltraLight"); 
              break; 
            case 0x400: 
              Serial.println("Mifare One (S50)"); 
              break; 
            case 0x200: 
              Serial.println("Mifare One (S70)"); 
              break; 
            case 0x800: 
              Serial.println("Mifare Pro (X)"); 
              break; 
            case 0x4403: 
              Serial.println("Mifare DESFire"); 
              break; 
            default: 
              Serial.println("Unknown"); 
              break; 
          } 
} 
 
 
//Anti-collision, return tag serial number 4 bytes 
status = myRFID.AddicoreRFID_Anticoll(str); 
if (status == MI_OK) 
{ 
          checksum1 = str[0] ^ str[1] ^ str[2] ^ str[3]; 
          Serial.print("The tag's number is:\t"); 
       Serial.print(str[0]); 
            Serial.print(" , "); 
       Serial.print(str[1]); 
            Serial.print(" , "); 
       Serial.print(str[2]); 
            Serial.print(" , "); 
       Serial.println(str[3]); 
 
 
          Serial.print("Read Checksum:\t\t"); 
         Serial.println(str[4]); 
          Serial.print("Calculated Checksum:\t"); 
            Serial.println(checksum1); 
             
            // Should really check all pairs, but for now we'll just use the first 
            if(str[0] == 158)                      //You can change this to the first byte of your tag by finding the card's ID through the Serial Monitor 
            { 
                LCD.println("\nHello Ami!\n"); 
            } else if(str[0] == 181) {             //You can change this to the first byte of your tag by finding the card's ID through the Serial Monitor 
                LCD.println("\nHello Krishna!\n"); 
            } 
            Serial.println(); 
            delay(1000); 
} 
        myRFID.AddicoreRFID_Halt();   //Command tag into hibernation
}
