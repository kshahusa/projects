#include <LiquidCrystal.h>
LiquidCrystal LCD(10,9,5,4,3,2);
int timer=0;
int ledPin=12;

void setup() {
  LCD.begin(16,2);
  LCD.setCursor(0,0);
  LCD.print("My Timer");
  pinMode(ledPin,OUTPUT);

}

void loop() {
  for (timer=1;timer<=10;timer=timer+1) {
   LCD.setCursor(0,1);
   LCD.print(timer);
   LCD.print(" Seconds");
   delay (1000); 
  }

  for (timer=1;timer>=10;timer=timer-1) {
   LCD.setCursor(0,1);
   LCD.print(timer);
   LCD.print(" Seconds");
   delay (1000); 
  }
  delay (60000);
  digitalWrite(ledPin,HIGH);
  while (Serial.available()==0) {}
}

