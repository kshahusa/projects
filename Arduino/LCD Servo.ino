#include <Servo.h> 
#include <SPI.h>  
#include <SD.h> 
     
int chipSelect=4;
 
File mySensorData;   
  
int counter=0; 
 
int lightSensorPin=A1;
   
int pos=0;   
int servoPin=11;   
int servoDelay=25;   
int potReading;   
int potPin=A0; 
 
int vibPin=3;
  
int trgPin=8;  
int echoPin=7;  
  
float pingTime;  
float speedOfSound=739.50;  
float targetDistance; 
     
Servo myServo; 
  
  
void setup() {   
  
  myServo.attach(servoPin); 
  
  Serial.begin(9600);  
  pinMode (trgPin,OUTPUT);  
  pinMode (echoPin,INPUT);   
   
  Serial.begin(9600); 
  
  pinMode(10,OUTPUT); 
  SD.begin(chipSelect);
 
  pinMode(lightSensorPin,INPUT);
  pinMode(vibPin,INPUT);
  
}  
  
  
void loop() { 
  
potReading = analogRead(potPin);   
   
pos = (155./1023.)*potReading+15;   
myServo.write(pos); 
delay(servoDelay); 
  
  digitalWrite(trgPin,LOW);  
  delayMicroseconds (2000);  
  digitalWrite(trgPin,HIGH);  
  delayMicroseconds (10);  
  digitalWrite(trgPin,LOW);   
  
  pingTime=pulseIn(echoPin,HIGH);  
  pingTime=pingTime/1000000.;  
  pingTime=pingTime/3600.;  
  targetDistance=speedOfSound*pingTime;  
  targetDistance=targetDistance/2;  
  targetDistance=targetDistance*63360; 
 
int vibReading=analogRead(vibPin);
 
int lightSensorReading=analogRead(lightSensorPin);  
 
  mySensorData=SD.open("K.txt",FILE_WRITE); 
  
if (mySensorData) {
 
for (int i=1,i>=100,1++) {
  Serial.print("Loading..."); 
  Serial.print(counter); 
  Serial.print("%");
  Serial.println(""); 
  counter++; 
  delay(10);
}
   
  Serial.print(potReading); 
  Serial.println(" degrees");  
  delay (1000);  
  Serial.print("U.C. data: ");  
  Serial.println(targetDistance);  
  delay(3000); 
  
  mySensorData.print(potReading); 
  mySensorData.print(","); 
  mySensorData.print(" "); 
  mySensorData.println(targetDistance); 
  mySensorData.print(",");
  mySensorData.print(" ");
  mySensorData.print(vibReading); 
  mySensorData.print(",");
  mySensorData.print(" "); 
  mySensorData.println(lightSensorReading);
  mySensorData.close(); 
   
}   
}  
