int redPin=8;
int greenPin=9;
int bluePin=10;
String color;

void setup() {
  Serial.begin(9600);
  pinMode (redPin,OUTPUT);
  pinMode (greenPin,OUTPUT);
  pinMode (bluePin,OUTPUT);  
}

void loop() {
   
   Serial.println("What color do you want the LED to be (red,green, blue, sky blue or white)?");
   while (Serial.available()==0){} 
   color=Serial.readString();
   if (color=="red") {
    analogWrite (greenPin,0);
    analogWrite (bluePin,0);
    analogWrite (redPin,255);
     }
   else if (color=="green") {
    analogWrite (redPin,0);
    analogWrite (bluePin,0);
    analogWrite (greenPin,255);
     }     
   else if (color=="blue") {
    analogWrite (redPin,0);
    analogWrite (greenPin,0);
    analogWrite (bluePin,255);
     }    
   else if (color=="sky blue") {
    analogWrite (redPin,6);
    analogWrite (greenPin,0);
    analogWrite (bluePin,1);      
   }
   else if (color=="white") {
    analogWrite (redPin,255);
    analogWrite (greenPin,255);
    analogWrite (bluePin,255);    
   }
    if (color!="red" && "blue" && "green" && "sky blue") {
      Serial.println ("");
      Serial.println("Error! This color is not valid! Try again! ");
      Serial.println ("");
    }
    }

