const int voltRead=4; 
  
void setup() { 
  
  pinMode(voltRead,INPUT); 
  Serial.begin(9600);
  
} 
  
void loop() { 
  
int volts; 
  
volts = digitalRead(voltRead); 
  
  Serial.println(volts); 
  delay(1000); 
  
} 
