#ONLY WORKS IF YOU RUN IT ON THE TERMINAL
#ALSO IF IT GIVE YOU AN ERROR ABOUT THE PATENT, RUN THIS
#pip3 uninstall opencv-python
#pip3 install -U opencv-contrib-python==3.4.2.16

import numpy as np
import cv2

cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
    cv2.imshow('frame',gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()