#https://www.youtube.com/watch?v=6g4O5UOH304

import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
import os

data = keras.datasets.fashion_mnist

(train_images, train_labels),  (test_images, test_labels) = data.load_data()

class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

train_images = train_images/255.0
test_images = test_images/255.0

#print(train_images[7])
#plt.imshow(train_images[7], cmap=plt.cm.binary)
#plt.show()

model = keras.Sequential([ #sequential says to list layers in sequence
    keras.layers.Flatten(input_shape=(28,28)), #Input layer setup. making data from [[1],[2],[3]] to [1,2,3]
    keras.layers.Dense(128, activation="relu"), #1st hidden layer of 128 neurons, Dense means all layers interconnected equally. Activation is the rectifier linear
    # unit making the proccesing complex. It does this by making all negetives 0 and all positives bigger.
    keras.layers.Dense(128, activation="relu"),
    keras.layers.Dense(10, activation="softmax")#out layer of 10 neurons, activation gives a probability for each of the 10 output choices
])

# Compile up

model.compile(optimizer="adam", loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True), metrics=["accuracy"])# loss function is where you look at the probability and
# change weights and biases based on how much you lost. metric is what you want to achieve.

#Train
checkpoint_path = "training_1/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)

# Create a callback that saves the model's weights
cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                 save_weights_only=True,
                                                 verbose=1)
model.fit(train_images,train_labels,epochs=5,callbacks=[cp_callback]) #epochs are how many times the network sees the same data during training. feeds data randomly. gives
#model.load_weights("training_1/cp.ckpt.index")
# vareity in the testing

#Run time

#test_loss, test_acc = model.evaluate(test_images,test_labels)
#print("Tested Acc: ", test_acc)
#print("Tested Acc: ", test_acc*100, "%")

#Use it

prediction = model.predict(test_images)
#prediction = model.predict([test_images[7]]) # to predict a particular image

for i in range(5):
    plt.grid(False)
    plt.imshow(test_images[i], cmap=plt.cm.binary)
    plt.xlabel("Actual: " + class_names[test_labels[i]])
    plt.title("Prediction: " + class_names[np.argmax(prediction[i])])
    plt.show()
#print(class_names[np.argmax(prediction[0])])