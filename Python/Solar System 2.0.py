from visual import *  
import numpy  
import time  
   
scene2 = display(title='Solar System',  
     x=0, y=0, width=600, height=200,  
     center=(5,0,0), background=(0,0,0))  
   
orbit3=ring(align='center', axis=(0,1,0), radius=3, thickness=0.01, make_trail=True)  
orbit2=ring(align='center', axis=(0,1,0), radius=2, thickness=0.01, make_trail=True)  
orbit1=ring(align='center', axis=(0,1,0), radius=1, thickness=0.01, make_trail=True) 
orbit7=ring(align='center', axis=(0,1,0), radius=8, thickness=0.01, make_trail=True)  
orbit6=ring(align='center', axis=(0,1,0), radius=6, thickness=0.01, make_trail=True)  
orbit5=ring(align='center', axis=(0,1,0), radius=5, thickness=0.01, make_trail=True) 
orbit4=ring(align='center', axis=(0,1,0), radius=4, thickness=0.01, make_trail=True)
orbit8=ring(align='center', axis=(0,1,0), radius=10, thickness=0.01, make_trail=True)
orbit9=ring(align='center', axis=(0,1,0), radius=11, thickness=0.01, make_trail=True)
saternRing=ring(pos=(8,0,0), axis=(0,1,0), radius=1, thickness=0.01, make_trail=True)
 
   
sun=sphere(color=color.yellow, pos=(0,0,0),radius=.7)
mercury=sphere(color=color.red, pos=(1,0,0),radius=.2)
venus=sphere(color=color.red, pos=(2,0,0),radius=.3)
earth=sphere(color=color.blue, pos=(3,0,0),radius=.4)
mars=sphere(color=color.red, pos=(4,0,0),radius=.5)
jupiter=sphere(color=color.red, pos=(6,0,0),radius=.6)
saturn=sphere(color=color.yellow, pos=(8,0,0),radius=.5)
uranus=sphere(color=color.blue, pos=(10,0,0),radius=.4)
neptune=sphere(color=color.blue, pos=(11,0,0),radius=.3)
pluto=sphere(color=color.white, pos=(13,0,0),radius=.1)
   
while 1==1 :  
    rate(10)  
    orbit1.rotate(angle=pi/4, axis=(0,1,0))  
    orbit2.rotate(angle=pi/4, axis=(0,1,0))  
    orbit3.rotate(angle=pi/4, axis=(0,1,0))  
    sun.rotate(angle=pi/4, axis=(1,0,1))  
  
 


