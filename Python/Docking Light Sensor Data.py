import serial #Import Serial Library
from visual import * #Import all the vPython library
 
arduinoSerialData = serial.Serial('com4', 9600) #Create an object for the Serial port. Adjust 'com11' to whatever port your arduino is sending to.
measuringRod = cylinder( radius=0.5, length=6, color=color.cyan, pos=(-3,-2,0))
target=box(pos=(0,-0.5,-2),color=color.magenta,height=6,width=3,length=0.2)
label=text(text='Distance is: ',
    pos=(-4,3,0), depth=-0.1, color=color.cyan)
 
while (1==1):  #Create a loop that continues to read and display the data
    rate(20)#Tell vpython to run this loop 20 times a second
    if (arduinoSerialData.inWaiting()>0):  #Check to see if a data point is available on the serial port
        myData = arduinoSerialData.readline() #Read the distance measure as a string
        print myData #Print the measurement to confirm things are working
        distance = float(myData) #convert reading to a floating point number
        target.pos=(-3+distance,-0.5,0)
        label.text='Distance is: ' + myData
        measuringRod.length=distance #Change the length of your measuring rod to your last measurement
        
                
 

