from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd

import pandas
train = pandas.read_csv('train.csv')
test = pandas.read_csv("eval.csv")
test['alone'] = test['alone'].map({'y': 1, 'n': 0})
print(test)

model = keras.Sequential([ #sequential says to list layers in sequence
    keras.layers.Input(test), #Input layer setup. making data from [[1],[2],[3]] to [1,2,3]
    keras.layers.Dense(128, activation="relu"), #1st hidden layer of 128 neurons, Dense means all layers interconnected equally. Activation is the rectifier linear
    # unit making the proccesing complex. It does this by making all negetives 0 and all positives bigger.
    keras.layers.Dense(128, activation="relu"),
    keras.layers.Dense(10, activation="softmax")#out layer of 10 neurons, activation gives a probability for each of the 10 output choices
])