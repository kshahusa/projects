import subprocess
import cv2
import numpy as np
from PIL import Image
import os
import argparse
import glob
import time
import sys

basedir = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(basedir, os.path.pardir)))
from tracker import re3_tracker

from re3_utils.util import drawing
from re3_utils.util import bb_util
from re3_utils.util import im_util

from constants import OUTPUT_WIDTH
from constants import OUTPUT_HEIGHT
from constants import PADDING

import sys
from PyQt5.QtWidgets import (QMainWindow, QWidget, QScrollArea, QApplication, QLineEdit,
                             QHBoxLayout, QVBoxLayout, QCompleter, QSpacerItem, QSizePolicy)
from PyQt5.QtCore import Qt

from PyQt5.QtWidgets import (QWidget, QLabel, QPushButton,
                             QHBoxLayout)

#capturemode = input("Capture Mode(cam, vid): ")
reference = "Input/SearchImage.jpg"
# sift = False
# back = False
# recog = False
# train = False
# select = False
# object = True
sift = False
back = False
recog = False
train = False
select = False
object = False
run = False
captureMode = "vid"


class OnOffWidget(QWidget):

    def __init__(self, name):
        super(OnOffWidget, self).__init__()

        self.name = name
        self.is_on = False

        self.lbl = QLabel(self.name)
        self.btn_on = QPushButton("On")
        self.btn_off = QPushButton("Off")

        self.hbox = QHBoxLayout()
        self.hbox.addWidget(self.lbl)
        self.hbox.addWidget(self.btn_on)
        self.hbox.addWidget(self.btn_off)

        self.btn_on.clicked.connect(self.on)
        self.btn_off.clicked.connect(self.off)

        self.setLayout(self.hbox)

        self.update_button_state()

    def show(self):
        """
        Show this widget, and all child widgets.
        """
        for w in [self, self.lbl, self.btn_on, self.btn_off]:
            w.setVisible(True)

    def hide(self):
        """
        Hide this widget, and all child widgets.
        """
        for w in [self, self.lbl, self.btn_on, self.btn_off]:
            w.setVisible(False)

    def off(self):
        self.is_on = False
        self.update_button_state()
        #print("off")
        global sift
        global back
        global recog
        global train
        global select
        global object
        global captureMode
        global run
        if (self.name == "sift"):
            sift = False
        elif (self.name == "back"):
            back = False
        elif (self.name == "recog"):
            recog = False
        elif (self.name == "train"):
            train = False
        elif (self.name == "select"):
            select = False
        elif (self.name == "object"):
            object = False
        elif (self.name == "capM"):
            captureMode = "vid"
        if (self.name == "run"):
            print(sift)
            print(back)
            print(recog)
            print(train)
            print(select)
            print(object)
            print(captureMode)
            #sys.exit(app.exec_())

    def on(self):
        self.is_on = True
        self.update_button_state()
        #print("on"+self.name)
        global sift
        global back
        global recog
        global train
        global select
        global object
        global captureMode
        global run
        if(self.name == "SIFT"):
            sift = True
        elif (self.name == "Background Reducer"):
            back = True
        elif (self.name == "Recognition"):
            recog = True
        elif (self.name == "Train Recognizer"):
            train = True
        elif (self.name == "Select Track"):
            select = True
        elif (self.name == "Object Detector"):
            object = True
        elif (self.name == "Capture Mode"):
            captureMode = "cam"
        elif(self.name == "Execute"):
            print(sift)
            print(back)
            print(recog)
            print(train)
            print(select)
            print(object)
            print(captureMode)
            run = True
            #app.quit()


    def update_button_state(self):
        """
        Update the appearance of the control buttons (On/Off)
        depending on the current state.
        """
        if self.is_on == True:
            self.btn_on.setStyleSheet("background-color: #4CAF50; color: #fff;")
            self.btn_off.setStyleSheet("background-color: none; color: none;")
        else:
            self.btn_on.setStyleSheet("background-color: none; color: none;")
            self.btn_off.setStyleSheet("background-color: #D32F2F; color: #fff;")

class MainWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super().__init__()

        self.controls = QWidget()  # Controls container widget.
        self.controlsLayout = QVBoxLayout()   # Controls container layout.

        # List of names, widgets are stored in a dictionary by these keys.
        widget_names = [
            "SIFT", "Background Reducer", "Recognition", "Train Recognizer",
            "Select Track", "Object Detector", "Capture Mode", "Execute"
        ]
        self.widgets = []

        # Iterate the names, creating a new OnOffWidget for
        # each one, adding it to the layout and
        # and storing a reference in the `self.widgets` dict
        for name in widget_names:
            item = OnOffWidget(name)
            self.controlsLayout.addWidget(item)
            self.widgets.append(item)

        spacer = QSpacerItem(1, 1, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.controlsLayout.addItem(spacer)
        self.controls.setLayout(self.controlsLayout)

        # Scroll Area Properties.
        self.scroll = QScrollArea()
        self.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.controls)

        # Search bar.
        self.searchbar = QLineEdit()
        self.searchbar.textChanged.connect(self.update_display)

        # Adding Completer.
        self.completer = QCompleter(widget_names)
        self.completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.searchbar.setCompleter(self.completer)

        # Add the items to VBoxLayout (applied to container widget)
        # which encompasses the whole window.
        container = QWidget()
        containerLayout = QVBoxLayout()
        containerLayout.addWidget(self.searchbar)
        containerLayout.addWidget(self.scroll)

        container.setLayout(containerLayout)
        self.setCentralWidget(container)

        self.setGeometry(600, 100, 800, 600)
        self.setWindowTitle('Control Panel')

    def update_display(self, text):
        global sift
        global back
        global recog
        global train
        global select
        global object

        for widget in self.widgets:
            if text.lower() in widget.name.lower():
                widget.show()
            else:
                widget.hide()


app = QApplication(sys.argv)
print("argv")
w = MainWindow()

print("window")
w.show()

print(run)
print("exiter")
text = input("type in enter")  # or raw_input in python2
if text == "":
    print("you pressed enter")
    #sys.exit(app.exec_())
    print("exit")
    app.exit()

else:
    print("you typed some text before pressing enter")
#sys.exit(app.exec_())
print("hi")
print("bye")


count = 0 #start
end = 10
names = ['None', 'Krishna', 'Ami', 'Ashray', 'Ankur', 'W']

np.set_printoptions(precision=6)
np.set_printoptions(suppress=True)

drawnBox = np.zeros(4)
boxToDraw = np.zeros(4)
mousedown = False
mouseupdown = False
initialize = False

font = cv2.FONT_HERSHEY_SIMPLEX

# iniciate id counter
id = 0

# names related to ids: example ==> Marcelo: id=1,  etc
names = ['None', 'Krishna', 'Ami', 'Ashray', 'Ankur', 'W']

# Define min window size to be recognized as a face



# Path for face image database
path = 'dataset'

qMode = False

detector = cv2.CascadeClassifier("haarcascade_frontalface_default.xml");
recognizer = cv2.face.LBPHFaceRecognizer_create()
face_detector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

def on_mouse(event, x, y, flags, params):
    global mousedown, mouseupdown, drawnBox, boxToDraw, initialize
    if event == cv2.EVENT_LBUTTONDOWN:
        drawnBox[[0,2]] = x
        drawnBox[[1,3]] = y
        mousedown = True
        mouseupdown = False
    elif mousedown and event == cv2.EVENT_MOUSEMOVE:
        drawnBox[2] = x
        drawnBox[3] = y
    elif event == cv2.EVENT_LBUTTONUP:
        drawnBox[2] = x
        drawnBox[3] = y
        mousedown = False
        mouseupdown = True
        initialize = True
    boxToDraw = drawnBox.copy()
    boxToDraw[[0,2]] = np.sort(boxToDraw[[0,2]])
    boxToDraw[[1,3]] = np.sort(boxToDraw[[1,3]])


def show_webcam(mirror=False):
    global tracker, initialize
    #cam = cv2.VideoCapture(0)
    cv2.namedWindow('Webcam', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Webcam', OUTPUT_WIDTH, OUTPUT_HEIGHT)
    cv2.setMouseCallback('Webcam', on_mouse, 0)
    frameNum = 0
    outputDir = None
    outputBoxToDraw = None
    if RECORD:
        print('saving')
        if not os.path.exists('outputs'):
            os.mkdir('outputs')
        tt = time.localtime()
        outputDir = ('outputs/%02d_%02d_%02d_%02d_%02d/' %
                (tt.tm_mon, tt.tm_mday, tt.tm_hour, tt.tm_min, tt.tm_sec))
        os.mkdir(outputDir)
        labels = open(outputDir + 'labels.txt', 'w')
    while True:
        ret_val, img = cap.read()
        if mirror:
            img = cv2.flip(img, 1)
        origImg = img.copy()
        if mousedown:
            cv2.rectangle(img,
                    (int(boxToDraw[0]), int(boxToDraw[1])),
                    (int(boxToDraw[2]), int(boxToDraw[3])),
                    [0,0,255], PADDING)
            if RECORD:
                cv2.circle(img, (int(drawnBox[2]), int(drawnBox[3])), 10, [255,0,0], 4)
        elif mouseupdown:
            if initialize:
                outputBoxToDraw = tracker.track('webcam', img[:,:,::-1], boxToDraw)
                initialize = False
            else:
                outputBoxToDraw = tracker.track('webcam', img[:,:,::-1])
            cv2.rectangle(img,
                    (int(outputBoxToDraw[0]), int(outputBoxToDraw[1])),
                    (int(outputBoxToDraw[2]), int(outputBoxToDraw[3])),
                    [0,0,255], PADDING)
        cv2.imshow('Webcam', img)
        if RECORD:
            if outputBoxToDraw is not None:
                labels.write('%d %.2f %.2f %.2f %.2f\n' %
                        (frameNum, outputBoxToDraw[0], outputBoxToDraw[1],
                            outputBoxToDraw[2], outputBoxToDraw[3]))
            cv2.imwrite('%s%08d.jpg' % (outputDir, frameNum), origImg)
            print('saving')
        keyPressed = cv2.waitKey(1)
        if keyPressed == 27 or keyPressed == 1048603:
            break  # esc to quit
        frameNum += 1
    cv2.destroyAllWindows()


def getImagesAndLabels(path):

    imagePaths = [os.path.join(path,f) for f in os.listdir(path)]
    faceSamples=[]
    ids = []

    for imagePath in imagePaths:

        PIL_img = Image.open(imagePath).convert('L') # convert it to grayscale
        img_numpy = np.array(PIL_img,'uint8')

        id = int(os.path.split(imagePath)[-1].split(".")[1])
        faces = detector.detectMultiScale(img_numpy)

        for (x,y,w,h) in faces:
            faceSamples.append(img_numpy[y:y+h,x:x+w])
            ids.append(id)

    return faceSamples,ids

def sift_detector(new_image, image_template):
    # Function that compares input image to template
    # It then returns the number of SIFT matches between them
    image1 = cv2.cvtColor(new_image, cv2.COLOR_BGR2GRAY)
    image2 = image_template

    # Create SIFT detector object
    #sift = cv2.SIFT()
    sift = cv2.xfeatures2d.SIFT_create()
    # Obtain the keypoints and descriptors using SIFT
    keypoints_1, descriptors_1 = sift.detectAndCompute(image1, None)
    keypoints_2, descriptors_2 = sift.detectAndCompute(image2, None)

    # Define parameters for our Flann Matcher
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 3)
    search_params = dict(checks = 100)

    # Create the Flann Matcher object
    flann = cv2.FlannBasedMatcher(index_params, search_params)

    # Obtain matches using K-Nearest Neighbor Method
    # the result 'matchs' is the number of similar matches found in both images
    matches = flann.knnMatch(descriptors_1, descriptors_2, k=2)

    # Store good matches using Lowe's ratio test
    good_matches = []
    for m,n in matches:
        if m.distance < 0.7 * n.distance:
            good_matches.append(m)
    return len(good_matches)

while(object == True):
    subprocess.call("object.bat", shell=True)

if(captureMode=="cam" and object == False):
    cap = cv2.VideoCapture(0)
elif(captureMode=="vid"):
    cap = cv2.VideoCapture("Input/input.mp4")
else:
    print("This is not a supported input. Sorry!")
_, first_frame = cap.read()
first_gray = cv2.cvtColor(first_frame, cv2.COLOR_BGR2GRAY)
first_gray = cv2.GaussianBlur(first_gray, (5, 5), 0)

image_template = cv2.imread(reference, 0)

minW = 0.1 * cap.get(3)
minH = 0.1 * cap.get(4)

while True:
    if(back == True):
        _, frame = cap.read()
        gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray_frame = cv2.GaussianBlur(gray_frame, (5, 5), 0)
        difference = cv2.absdiff(first_gray, gray_frame)
        _, difference = cv2.threshold(difference, 25, 255, cv2.THRESH_BINARY)

        #cv2.imshow("First frame", first_frame)
        #cv2.imshow("Frame", frame)
        cv2.imshow("difference", difference)

    if(sift == True):
        # Get webcam images
        ret, frame = cap.read()

        # Get height and width of webcam frame
        height, width = frame.shape[:2]

        # Define ROI Box Dimensions
        top_left_x = int(width / 100)
        top_left_y = int((height / 1.37) + (height / 4))
        bottom_right_x = int((width / 3) * 3)
        bottom_right_y = int((height / 3.6) - (height / 4))

        # Draw rectangular window for our region of interest
        cv2.rectangle(frame, (top_left_x, top_left_y), (bottom_right_x, bottom_right_y), 255, 3)

        # Crop window of observation we defined above
        cropped = frame[bottom_right_y:top_left_y, top_left_x:bottom_right_x]

        # Flip frame orientation horizontally
        frame = cv2.flip(frame, 1)

        # Get number of SIFT matches
        matches = sift_detector(cropped, image_template)

        # Display status string showing the current no. of matches
        cv2.putText(frame, str(matches), (450, 450), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 255, 0), 1)

        # Our threshold to indicate object deteciton
        # We use 10 since the SIFT detector returns little false positves
        threshold = 10

        # If matches exceed our threshold then object has been detected
        if matches > threshold:
            cv2.rectangle(frame, (top_left_x, top_left_y), (bottom_right_x, bottom_right_y), (0, 255, 0), 3)
            cv2.putText(frame, 'Object Found', (50, 50), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 255, 0), 2)
        cv2.imshow('Object Detector using SIFT', frame)

    if(train==True):
        if(qMode == False):
            # For each person, enter one numeric face id
            face_id = input('\n enter user id end press <return> ==>  ')

            print("\n [INFO] Initializing face capture. Look the camera and wait ...")
            # Initialize individual sampling face count
            qMode = True
        else:

            ret, img = cap.read()
            # img = cv2.flip(img, -1) # flip video image vertically
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            faces = face_detector.detectMultiScale(gray, 1.3, 5)

            for (x, y, w, h) in faces:
                cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
                count += 1

                # Save the captured image into the datasets folder
                cv2.imwrite("dataset/User." + str(face_id) + '.' + str(count) + ".jpg", gray[y:y + h, x:x + w])

                cv2.imshow('image', img)
            if count >= end:  # end Take 30 face sample and stop video
                train = True
                break

    if(recog == True):
        recognizer.read('trainer/trainer.yml')
        cascadePath = "haarcascade_frontalface_default.xml"
        faceCascade = cv2.CascadeClassifier(cascadePath);
        ret, img = cap.read()
        # img = cv2.flip(img, -1) # Flip vertically

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.2,
            minNeighbors=5,
            minSize=(int(minW), int(minH)),
        )

        for (x, y, w, h) in faces:

            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)

            id, confidence = recognizer.predict(gray[y:y + h, x:x + w])

            # Check if confidence is less them 100 ==> "0" is perfect match
            if (confidence < 100):
                id = names[id]
                confidence = "  {0}%".format(round(100 - confidence))
            else:
                id = "unknown"
                confidence = "  {0}%".format(round(100 - confidence))

            cv2.putText(img, str(id), (x + 5, y - 5), font, 1, (255, 255, 255), 2)
            cv2.putText(img, str(confidence), (x + 5, y + h - 5), font, 1, (255, 255, 0), 1)

        cv2.imshow('camera', img)

        k = cv2.waitKey(10) & 0xff  # Press 'ESC' for exiting video
        if k == 27:
            break

    if(select == True):
        parser = argparse.ArgumentParser(
            description='Show the Webcam demo.')
        parser.add_argument('-r', '--record', action='store_true', default=False)
        args = parser.parse_args()
        RECORD = args.record

        tracker = re3_tracker.Re3Tracker()

        show_webcam(mirror=True)

    if(object == True):
        subprocess.call("object.bat", shell=True)
        object = False

    if(sift == False and back == False and train == False and recog == False):
        print("You haven't selected a mode")
        break

    key = cv2.waitKey(30)

    if key == 27:
        break

print("\n [INFO] Training faces. It will take a few seconds. Wait ...")
faces, ids = getImagesAndLabels(path)
recognizer.train(faces, np.array(ids))
recognizer.write('trainer/trainer.yml')  # recognizer.save() worked on Mac, but not on Pi
print("\n [INFO] {0} faces trained. Exiting Program".format(len(np.unique(ids))))

# Do a bit of cleanup
print("\n [INFO] Exiting Program and cleanup stuff")


cap.release()
cv2.destroyAllWindows()