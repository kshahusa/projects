from visual import * 
import numpy 
import time 
  
scene2 = display(title='Solar System', 
     x=0, y=0, width=600, height=200, 
     center=(5,0,0), background=(0,0,0)) 
  
orbit1=ring(align='center', axis=(0,1,0), radius=3, thickness=0.01, make_trail=True) 
orbit2=ring(align='center', axis=(0,1,0), radius=2, thickness=0.01, make_trail=True) 
orbit3=ring(align='center', axis=(0,1,0), radius=1, thickness=0.01, make_trail=True) 
  
sun=sphere(color=color.yellow, pos=(0,0,0),radius=.6) 
  
#pointer = arrow(pos=(0,2,1),axis=(5,0,0), shaftwidth=1)
v1=vector(0,0,0)
v2=vector(1,0,0)
  
  
while 1==1 : 
    rate(10) 
    orbit1.rotate(angle=pi/4, axis=(0,1,0)) 
    orbit2.rotate(angle=pi/4, axis=(0,1,0)) 
    orbit3.rotate(angle=pi/4, axis=(0,1,0)) 
    sun.rotate(angle=pi/4, axis=(1,0,1)) 
    #pointer.rotate(angle=pi/4, axis=(1,0,1))
    sun.pos=(v1+v2)
    time.sleep(1)
    sun.pos=(v1-v2)

from visual import * 
import numpy 
import time 
  
scene2 = display(title='Solar System', 
     x=0, y=0, width=600, height=200, 
     center=(5,0,0), background=(0,0,0)) 
  
orbit1=ring(align='center', axis=(0,1,0), radius=3, thickness=0.01, make_trail=True) 
orbit2=ring(align='center', axis=(0,1,0), radius=2, thickness=0.01, make_trail=True) 
orbit3=ring(align='center', axis=(0,1,0), radius=1, thickness=0.01, make_trail=True)
neptune=sphere(color=color.cyan,pos=(-3,-1,0),radius=.3)
  
sun=sphere(color=color.yellow, pos=(0,0,0),radius=.6)
 
t=text(text='This is the Solar System',
    pos=(-6,3,0), depth=-0.3, color=color.green)
 
scene1 = display(title='Solar System',
     x=0, y=0, width=600, height=200,
     center=(5,0,0), background=(0,1,1))
neptune.velocity = vector(0,-1,0)
dt = 0.01
  
while 1==1 : 
    rate(10) 
    orbit1.rotate(angle=pi/4, axis=(0,1,0)) 
    orbit2.rotate(angle=pi/4, axis=(0,1,0)) 
    orbit3.rotate(angle=pi/4, axis=(0,1,0)) 
    sun.rotate(angle=pi/4, axis=(1,0,1))
    neptune.pos = neptune.pos + neptune.velocity*dt
    if neptune.y < neptune.radius:
        neptune.velocity.y = abs(neptune.velocity.y)
    else:
        neptune.velocity.y = neptune.velocity.y - 9.8*dt
     

    
